﻿using PlanPaddy.Core.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Messaging.Email
{
    public class UserMailer : IUserMailer
    {

        public  MailMessage _mail;
        public  SmtpClient _smtpServer;
    

        public UserMailer()
        {


            EmailParamsDto credentials = new EmailParamsDto();
            //to be moved to web.config
            credentials.UserName = "hello@planpaddy.com";
            credentials.Password = "T3m!t0p3";
            credentials.Host = "relay-hosting.secureserver.net";
            credentials.UseSsl = false;




            MailMessage mail = new MailMessage();



            SmtpClient smtpServer = new SmtpClient
            {
                Credentials = new System.Net.NetworkCredential
                     (credentials.UserName, credentials.Password),
                //Port = credentials.Port,
                Host = credentials.Host,
                EnableSsl = false,
                
                DeliveryMethod = SmtpDeliveryMethod.Network,

                //UseDefaultCredentials = false                 
            };
            smtpServer.ServicePoint.MaxIdleTime = 5;
            smtpServer.ServicePoint.ConnectionLimit = 1;
          //  smtpServer.ServicePoint.UseNagleAlgorithm = true;
          
            smtpServer.Port = 25;
            
            _smtpServer = smtpServer;
            mail.From = new MailAddress("hello@PlanPaddy.com",
                "PlanPaddy", System.Text.Encoding.UTF8);
            mail.IsBodyHtml = true;
            _mail = mail;
        }



        public void SendMail(string mailTo, string subject, string body)
        {
            _mail.To.Clear();
            _mail.To.Add(mailTo);
            _mail.Subject = subject;
            _mail.Body = body;

            _smtpServer.Send(_mail);

        }

        public void SendMailMultiple(List<string> mailTo, string subject, string body)
        {
            _mail.To.Clear();

            if (mailTo != null)
            {
                foreach (var mail in mailTo)
                {
                    // _mail.To.Clear();
                    _mail.To.Add(mail);
                }

                _mail.Subject = subject;
                _mail.Body = body;

                _smtpServer.Send(_mail);
            }


        }

        public void SendHtmlMail(string mailTo, string subject, string body)
        {
            _mail.To.Clear();
            _mail.To.Add(mailTo);
            _mail.Subject = subject;
            _mail.Body = body;
            _mail.IsBodyHtml = true;

            _smtpServer.Send(_mail);

        }

    }
}
