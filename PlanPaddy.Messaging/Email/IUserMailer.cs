﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Messaging.Email
{
   public interface IUserMailer
    {
        void SendMail(string mailTo, string subject, string body);
        void SendMailMultiple(List<string> mailTo, string subject, string body);

        void SendHtmlMail(string mailTo, string subject, string body);
    }
}
