﻿using Microsoft.AspNet.Identity;
using PlanPaddy.Core.Common.Extensions;

namespace PlanPaddy.Services.Security
{
    public class PlanPaddyHasher: IPasswordHasher
    {
        public string HashPassword(string password)
        {
            return password.Encrypt();
        }

        public PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            if (hashedPassword != HashPassword(providedPassword))
                return PasswordVerificationResult.Failed;

            return PasswordVerificationResult.Success;
        }

    }
}
