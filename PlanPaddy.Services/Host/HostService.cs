﻿using PlanPaddy.Core.Cryptography;
using PlanPaddy.Core.DTOs;
using PlanPaddy.Core.Persistence;
using PlanPaddy.Core.Services.Host;
using PlanPaddy.Messaging.Email;
using PlanPaddy.Services.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Services.Host
{
    public class HostService: IHostService {
        private readonly IUnitOfWork _uow;
        private readonly IUserMailer _userMailer;
        private readonly PlanPaddyHasher _passwordHasher;
        public HostService(IUnitOfWork uow, IUserMailer userMailer)
        {
            //_Repo = Repo;

            _uow = uow;
            _userMailer = userMailer;
        }

        public async Task<HostDto> GetHost(string HostRefcode)
        {
            var host = await _uow.Hosts.GetHost(HostRefcode);
            var returnedHost = new HostDto
            {
                HostId = host.HostId,
                Name = host.Name,
                FirstName = host.FirstName,
                LastName = host.LastName,
                About = host.About,
                Picture = host.Picture,
                EmailAddress = host.EmailAddress,
                MobileNo = host.MobileNo,
                IsActive = host.IsActive,
                RefCode = host.RefCode,
                AccountIsConfirmed = host.AccountIsConfirmed,
                Facebook = host.Facebook,
                Twitter = host.Twitter,
                Instagram = host.Instagram
            };
           
            return returnedHost;
        }

        public async Task<HostDto> SaveHost(HostDto hostDto)
        {
            if (hostDto == null)
                throw new Exception("Profile required, null value received");
            var existing = await _uow.Hosts.GetHost(hostDto.RefCode);
            if (existing == null)
            {
                try
                {
                    var encryptedPass = CryptManager.Encrypt(hostDto.Password);
                    hostDto.Password = encryptedPass;
                    var savedHost = await _uow.Hosts.SaveHost(hostDto);
                    var hostDetails = await GetHost(savedHost.RefCode);
                  //  await SendConfirmationCode(vendorDetails);

                    //  var vendorItemConfRes = await _uow.Vendors.SaveVendorItems(vendorDto.VendorItems);

                    return hostDetails;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            else
            {
                throw new Exception("Account already exists");
            }

        }
    }
    
}
