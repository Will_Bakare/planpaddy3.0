﻿using PlanPaddy.Core;
using PlanPaddy.Core.Common.Helpers;
using PlanPaddy.Core.Cryptography;
using PlanPaddy.Core.DTOs;
using PlanPaddy.Core.Persistence;
using PlanPaddy.Core.Services.Vendor;
using PlanPaddy.Messaging.Email;
using PlanPaddy.Services.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using RazorEngine;

namespace PlanPaddy.Services.Vendor
{
    public class VendorService :IVendorService
    {
        private readonly IUnitOfWork _uow;
        private readonly IUserMailer _userMailer;
        private readonly PlanPaddyHasher _passwordHasher;
        public VendorService(IUnitOfWork uow, IUserMailer userMailer)
        {
            //_Repo = Repo;

            _uow = uow;
            _userMailer = userMailer;
        }
        public async Task<VendorDto> EditVendor(VendorDto vendorDto)
        {
            var updatedVendor = await _uow.Vendors.EditVendor(vendorDto);
            var returnedVendor = new VendorDto
            {
                Id = updatedVendor.VendorId,
                Name = updatedVendor.Name,
                Description = updatedVendor.Description,
                Picture = updatedVendor.Picture,
                EmailAddress = updatedVendor.EmailAddress,
                MobileNo = updatedVendor.MobileNo,
                IsActive = updatedVendor.IsActive,
                RefCode = updatedVendor.RefCode,
                Facebook = updatedVendor.Facebook,
                Twitter = updatedVendor.Twitter,
                Instagram = updatedVendor.Instagram,
                FirstName = updatedVendor.FirstName,
                LastName = updatedVendor.LastName
            };

            return returnedVendor;

        }

        public async Task<List<VendorDto>> GetAllVendors()
        {
            var active = await _uow.Vendors.GetAllActiveVendors();
            var inActive = await _uow.Vendors.GetAllInActiveVendors();
            var allVendors = new List<VendorDto>();
            foreach (var vendor in inActive)
            {

                var returnedVendor = new VendorDto
                {
                    Id = vendor.VendorId,
                    Name = vendor.Name,
                    Description = vendor.Description,
                    Picture = vendor.Picture,
                    EmailAddress = vendor.EmailAddress,
                    MobileNo = vendor.MobileNo,
                    IsActive = vendor.IsActive,
                    RefCode = vendor.RefCode,
                    Facebook = vendor.Facebook,
                    Twitter = vendor.Twitter,
                    Instagram = vendor.Instagram,
                    FirstName = vendor.FirstName,
                    LastName = vendor.LastName
                };
                allVendors.Add(returnedVendor);
            }
            foreach (var vendor in active)
            {
                var returnedVendor = new VendorDto
                {
                    Id = vendor.VendorId,
                    Name = vendor.Name,
                    Description = vendor.Description,
                    Picture = vendor.Picture,
                    EmailAddress = vendor.EmailAddress,
                    MobileNo = vendor.MobileNo,
                    IsActive = vendor.IsActive,
                    RefCode = vendor.RefCode,
                    Facebook = vendor.Facebook,
                    Twitter = vendor.Twitter,
                    Instagram = vendor.Instagram,
                    FirstName = vendor.FirstName,
                    LastName = vendor.LastName,
                };
                allVendors.Add(returnedVendor);

            }

            return allVendors;
        }


        public async Task<VendorDto> GetVendorDetailsByRefCode(string refCode)
        {
            var vendor = await _uow.Vendors.GetVendorByRefCode(refCode);
            var returnedVendor = new VendorDto
            {
                Id = vendor.VendorId,
                Name = vendor.Name,
                FirstName = vendor.FirstName,
                LastName = vendor.LastName,
                Description = vendor.Description,
                Picture = vendor.Picture,
                EmailAddress = vendor.EmailAddress,
                MobileNo = vendor.MobileNo,
                IsActive = vendor.IsActive,
                RefCode = vendor.RefCode,
                AccountIsConfirmed=vendor.AccountIsConfirmed,
                Facebook = vendor.Facebook,
                Twitter = vendor.Twitter,
                Instagram = vendor.Instagram
            };
            var vendorItems = await _uow.Vendors.GetVendorItemsByVendorId(vendor.VendorId);

            var vendorItemToReturn = new List<VendorItemDto>();
            foreach (var vendorItem in vendorItems)
            {
                var vendorItemDt = new VendorItemDto
                {
                    ItemId = vendorItem.ItemId,
                    VendorId = vendorItem.VendorId,
                    CategoryId = vendorItem.CategoryId,
                    LowerLimit = vendorItem.LowerLimit,
                    Quantity = vendorItem.Quantity,
                    UpperLimit = vendorItem.UpperLimit,
                    IsActive = true
                };
                vendorItemToReturn.Add(vendorItemDt);
            }
            returnedVendor.VendorItems = vendorItemToReturn;
            return returnedVendor;
        }
        public async Task<VendorDto> GetVendorDetailsByEmail(string email)
        {
            var vendor = await _uow.Vendors.GetVendorByEmail(email);
            var returnedVendor = new VendorDto
            {
                Id = vendor.VendorId,
                Name = vendor.Name,
                Description = vendor.Description,
                Picture = vendor.Picture,
                EmailAddress = vendor.EmailAddress,
                MobileNo = vendor.MobileNo,
                IsActive = vendor.IsActive,
                RefCode = vendor.RefCode,
                AccountIsConfirmed=vendor.AccountIsConfirmed,
                FirstName= vendor.FirstName,
                LastName=vendor.LastName,
                Facebook=vendor.Facebook,
                Twitter=vendor.Twitter,
                Instagram=vendor.Instagram
            };
            var vendorItems = await _uow.Vendors.GetVendorItemsByVendorId(vendor.VendorId);

            var vendorItemToReturn = new List<VendorItemDto>();
            foreach (var vendorItem in vendorItems)
            {
                var vendorItemDt = new VendorItemDto
                {
                    ItemId = vendorItem.ItemId,
                    VendorId = vendorItem.VendorId,
                    CategoryId = vendorItem.CategoryId,
                    LowerLimit = vendorItem.LowerLimit,
                    Quantity = vendorItem.Quantity,
                    UpperLimit = vendorItem.UpperLimit,
                    IsActive = true
                };
                vendorItemToReturn.Add(vendorItemDt);
            }
            returnedVendor.VendorItems = vendorItemToReturn;
            return returnedVendor;
        }
        public async Task<VendorDto> PostVendor(VendorPostRequest vendorDto)
        {
            if (vendorDto == null)
                throw new Exception("Profile required, null value received");
            var existing = await _uow.Vendors.FindExistingVendor(vendorDto.EmailAddress);
            if(existing == null)
            {
                try
                {
                    var encryptedPass = CryptManager.Encrypt(vendorDto.Password);
                    vendorDto.Password = encryptedPass;
                    var savedVendor = await _uow.Vendors.SaveVendor(vendorDto);
                    var vendorDetails = await GetVendorDetailsByRefCode(savedVendor.RefCode);
                    await SendConfirmationCode(vendorDetails);

                  //  var vendorItemConfRes = await _uow.Vendors.SaveVendorItems(vendorDto.VendorItems);

                    return vendorDetails;
                }
               catch(Exception e)
                {
                    return null;
                }
            }
            else
            {
                throw new Exception("Account already exists");
            }
            

        }


        public async Task<VendorDto> ResendVerificationCode(VendorPostRequest vendorDto)
        {
            if (vendorDto.EmailAddress == null)
                throw new Exception("Profile required, null value received");
            var existing = await _uow.Vendors.FindExistingVendor(vendorDto.EmailAddress);
            if (existing != null)
            {
                try
                {
                    var vendorDetails = await GetVendorDetailsByRefCode(existing.RefCode);
                    await SendConfirmationCode(vendorDetails);
                    return vendorDetails;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            else
            {
                throw new Exception("Account does not exists");
            }


        }




        public async Task<VendorDto> PostVendorItems(VendorDto vendorDto)
        {
            if (vendorDto == null)
                throw new Exception("Profile required, null value received");
            var existing = await _uow.Vendors.FindExistingVendor(vendorDto.EmailAddress);
           

            if (existing != null)
            {
                foreach (var vendItem in vendorDto.VendorItems)
                {
                    vendItem.VendorId = existing.VendorId;
                }
                try
                {
                    var vendorItemConfRes = await _uow.Vendors.SaveVendorItems(vendorDto.VendorItems);
                    var vendorDetails = await GetVendorDetailsByRefCode(existing.RefCode);
                    return vendorDetails;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            else
            {
                throw new Exception("Account already exists");
            }
        }


        public async Task<VendorDto> SignIn(VendorDto vendorDto)
        {
            if (vendorDto == null)
            {
                throw new Exception("Profile required, null value received");
            }
            var encPass =  CryptManager.Encrypt(vendorDto.Password);
            var existingVendor = await _uow.Vendors.SignIn(vendorDto.EmailAddress,encPass);
            if (existingVendor != null)
            {
                var returnedVendor = new VendorDto
                {
                    Id = existingVendor.VendorId,
                    Name = existingVendor.Name,
                    FirstName = existingVendor.FirstName,
                    LastName = existingVendor.LastName,
                    Description = existingVendor.Description,
                    Picture = existingVendor.Picture,
                    EmailAddress = existingVendor.EmailAddress,
                    MobileNo = existingVendor.MobileNo,
                    IsActive = existingVendor.IsActive,
                    RefCode = existingVendor.RefCode,
                    AccountIsConfirmed = existingVendor.AccountIsConfirmed,
                    Facebook = existingVendor.Facebook,
                    Twitter = existingVendor.Twitter,
                    Instagram = existingVendor.Instagram,
                    IsTempPassword = existingVendor.IsTempPassword

                };
                var vendorItems = await _uow.Vendors.GetVendorItemsByVendorId(existingVendor.VendorId);

                var vendorItemToReturn = new List<VendorItemDto>();
                foreach (var vendorItem in vendorItems)
                {
                    var vendorItemDt = new VendorItemDto
                    {
                        ItemId = vendorItem.ItemId,
                        VendorId = vendorItem.VendorId,
                        CategoryId = vendorItem.CategoryId,
                        LowerLimit = vendorItem.LowerLimit,
                        Quantity = vendorItem.Quantity,
                        UpperLimit = vendorItem.UpperLimit,
                        IsActive = true
                    };
                    vendorItemToReturn.Add(vendorItemDt);
                }
                returnedVendor.VendorItems = vendorItemToReturn;
                return returnedVendor;
            }
            else
            {
                throw new Exception("Invalid Login details");
            }
        }


        public async Task<bool> SendConfirmationCode(VendorDto vend)
        {
   
            if (vend==null)
            {
                throw new Exception("Username is required");
            }
            var vendor = await _uow.Vendors.GetVendorByEmail(vend.EmailAddress);
            if (vendor == null)
                throw new Exception("User Account does not exist");

            vend.OTP = SequentialGuid.NewGuid().ToString().Substring(0, 8);
            vend.RefCode = vendor.RefCode;
            await _uow.Vendors.UpdateVendorOtp(vend);
            try
            {
                // Send SMS
                // _smsNotification.SendSmsNowAsync("PlanPaddy OTP Code: " + customer.AccountConfirmationCode, new[] { customer.PhoneNumber }, "GIGMS");
                // Send Email
                if (!string.IsNullOrEmpty(vendor.EmailAddress))
                {

                
                // _userMailer.SendMail(vendor.EmailAddress, "PlanPaddy OTP", "Welcome to PlanPaddy" + Environment.NewLine + "Your PlanPaddy Activation Code:" + vendor.OTP);



                string template = File.ReadAllText(
                HttpContext.Current.Server.MapPath("/MailTemplate/") + "VerificationMail.cshtml");
                var body = Razor.Parse<VendorDto>(template, vend);

                string imgUrl = "http://beta.planpaddy.com/img/PlanPaddyLogo.png";
                body = body.Replace("IMAGEEMBED", imgUrl).Replace("Name", vend.FirstName).Replace("VerificationCode", vendor.OTP).Replace("Company", vendor.Name);
             //   _userMailer.SendHtmlMail(vendor.EmailAddress, "PlanPaddy Email Verification!", body);

            }

            }
            catch(Exception e)
            {

            }

            return true;
        }




        public async Task<VendorDto> MocMail(VendorDto vendor)
        {
            string template = string.Empty;

            try
            {
                var vend = new VendorDto();
           
                    template = File.ReadAllText(
                    HttpContext.Current.Server.MapPath("/MailTemplate/") + "VerificationMail.cshtml");
                    var body = Razor.Parse<VendorDto>(template, vend);

                    string imgUrl = "http://beta.planpaddy.com/img/PlanPaddyLogo.png";
                    body = body.Replace("IMAGEEMBED", imgUrl).Replace("Name", vend.FirstName).Replace("VerificationCode", vendor.OTP).Replace("Company", vendor.Name);
                    _userMailer.SendHtmlMail(vendor.EmailAddress, "PlanPaddy Email Verification!", body);
            }
            catch (Exception e)
            {
                //vendor.Facebook = e.StackTrace;
                //vendor.Twitter = e.Message;
                //vendor.Instagram = e.InnerException.Message;
                throw e;

            }
            vendor.Description = template;
            return vendor;

        }


        public async Task<VendorDto> ConfirmActivationCode(VendorDto vendorDto)
        {
            if (vendorDto==null)
            {
                throw new Exception("Invalid request,values can't be null");
            }
            var vendorFound = await _uow.Vendors.FindExistingVendor(vendorDto.EmailAddress);
            if (vendorFound != null)
            {
                if (vendorDto.OTP == vendorFound.OTP)
                {
                    vendorDto.AccountIsConfirmed = true;
                    await _uow.Vendors.UpdateVendorOtp(vendorDto);
                    //await _uow.CompleteAsync();
                }
                var vendorDetails = await GetVendorDetailsByRefCode(vendorFound.RefCode);
                return vendorDetails;
            }
            return null;
        }

        public async Task<VendorDto> UpdateVendorDetails(VendorDto vendorDto)
        {
            if (vendorDto == null)
            {
                throw new Exception("Invalid request,values can't be null");
            }
            var vendorFound = await _uow.Vendors.FindExistingVendor(vendorDto.EmailAddress);
            if (vendorFound != null)
            {
                //vendorDto.MobileNo = vendorDto.MobileNo;
                //vendorDto.Description = vendorDto.Description;
                //vendorDto.Twitter = vendorDto.Twitter;
                //vendorDto.Facebook = vendorDto.Facebook;
                //vendorDto.Instagram = vendorDto.Instagram;
                await _uow.Vendors.UpdateVendorDetails(vendorDto);
            }
            var vendorDetails = await GetVendorDetailsByRefCode(vendorFound.RefCode);
            return vendorDetails;
        }
        

        public async Task<VendorDto> RequestResetPassword(VendorDto vendorDto)
        {
            if (vendorDto.EmailAddress == null)
            {
                throw new Exception("Invalid request,values can't be null");
            }
            var vendorFound = await _uow.Vendors.FindExistingVendor(vendorDto.EmailAddress);
            if (vendorFound != null)
            {
                var NewPassword = SequentialGuid.NewGuid().ToString().Substring(0, 8);
                var encryptedPass = CryptManager.Encrypt(NewPassword);
                vendorDto.Password = encryptedPass;
                vendorDto.IsTempPassword = true;
                var vedorDetails =  await _uow.Vendors.ResetPassord(vendorDto);
                //mail sender
                string template = File.ReadAllText(
                HttpContext.Current.Server.MapPath("/MailTemplate/") + "PasswordResetMail.cshtml");
                var body = Razor.Parse<VendorDto>(template, vendorDto);
                string imgUrl = "http://beta.planpaddy.com/img/PlanPaddyLogo.png";
                body = body.Replace("IMAGEEMBED", imgUrl).Replace("Name", vedorDetails.FirstName).Replace("TempPassword", NewPassword);
                _userMailer.SendHtmlMail(vendorDto.EmailAddress, "PlanPaddy Password Reset!", body);
            }
            var vendorDetails = await GetVendorDetailsByRefCode(vendorFound.RefCode);
            return vendorDetails;
        }

        public async Task<VendorDto> ResetPassword(VendorDto vendorDto)
        {
            if (vendorDto.EmailAddress == null || vendorDto.Password == null)
            {
                throw new Exception("Invalid request,values can't be null");
            }
            var vendorFound = await _uow.Vendors.FindExistingVendor(vendorDto.EmailAddress);
            if (vendorFound != null)
            {
                var encryptedPass = CryptManager.Encrypt(vendorDto.Password);
                vendorDto.Password = encryptedPass;
                var vedorDetails = await _uow.Vendors.ResetPassord(vendorDto);
            }
            var vendorDetails = await GetVendorDetailsByRefCode(vendorFound.RefCode);
            return vendorDetails;
        }


    }
}
