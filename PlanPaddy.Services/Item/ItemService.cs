﻿using Newtonsoft.Json;
using PlanPaddy.Core.DTOs;
using PlanPaddy.Core.Entities.Item;
using PlanPaddy.Core.Persistence;
using PlanPaddy.Core.Services.Item;
using PlanPaddy.Messaging.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace PlanPaddy.Services.Item
{
    public class ItemService : IItemService
    {
        //private readonly IRepository<AXAM.Core.Entities.Item.Item> _Repo;
       // private readonly IItemRepository _itemRepo;
        private readonly IUnitOfWork _uow;
        private readonly IUserMailer _userMailer;
        public ItemService(IUnitOfWork uow , IUserMailer userMailer)
        {
            //_Repo = Repo;

            _uow = uow;
            _userMailer = userMailer;
        }

        public Task<CategoryDto> EditCategory(GetQuoteRequestModel getQuoteRequestModel)
        {
            throw new NotImplementedException();
        }

        public Task<ItemDto> EditItem(GetQuoteRequestModel getQuoteRequestModel)
        {
            throw new NotImplementedException();
        }

       

        public async Task<GetCategoryResponseDto> GetAllActiveCategories()
        {
            var activeCategories = await _uow.Items.GetAllActiveCategories();
            return activeCategories;
        }

        public async Task<PriceRangeResDTO> GetAllPriceRange()
        {
            var activePrices = await _uow.Items.GetAllPriceRange();
            return activePrices;
        }
        public async Task<List<ItemDto>> GetAllItems()
        {
            var activeItems =await  _uow.Items.GetAllItems();
            var items = new List<ItemDto>();
            foreach (var item in activeItems)
            {
                var returnedItem = new ItemDto
                {
                    ItemId = item.ItemId,
                    Name = item.Name,
                    ImageUrl=item.ImageUrl
                };
                items.Add(returnedItem);
            }
            return items;
        }

       

        public Task<CategoryDto> GetCategoryById(GetQuoteRequestModel getQuoteRequestModel)
        {
            throw new NotImplementedException();
        }

        public Task<ItemDto> GetItemById(GetQuoteRequestModel getQuoteRequestModel)
        {
            throw new NotImplementedException();
        }

        //Done
        public async Task<List<ItemDto>> GetQuote(GetQuoteRequestModel getQuoteRequestModel)
        {
            try
            {
                //trips.FirstOrDefault(t => t.ParentTripId == bus.TripId);
                var ItemList = await _uow.Items.GetItemsConfigurationByCategoryId(getQuoteRequestModel.CategoryId);
                VendorItemRequestModel vendorItemRequestModel = new VendorItemRequestModel();

                foreach (var item in ItemList)
                {
                    item.LowerLimit = (Convert.ToDouble(item.ItemPercentage) * getQuoteRequestModel.LowerLimit) / 100;
                    item.UpperLimit = (Convert.ToDouble(item.ItemPercentage) * getQuoteRequestModel.UpperLimit) / 100;
                    vendorItemRequestModel.ItemId = item.ItemId;
                    vendorItemRequestModel.LowerLimit = item.LowerLimit;
                    vendorItemRequestModel.UpperLimit = item.UpperLimit;
                    var VendorList = await _uow.Items.GetVendorItemConfiguration(vendorItemRequestModel);
                    item.Vendors = VendorList;


                }

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var ItemListString= serializer.Serialize(ItemList);
                // = ItemList.ToString();
                var saveQuoteRes = await SaveQuote(ItemListString);
                if (saveQuoteRes == true)
                {
                    return ItemList;
                }
                else
                {
                    return null;
                }
            }
            catch(Exception e)
            {
                throw e;
            }
            
           
        }

       

        public Task<CategoryDto> PostCategory(GetQuoteRequestModel getQuoteRequestModel)
        {
            throw new NotImplementedException();
        }

        public async Task<string> PostItem(ItemDto itemDto)
        {
            var savedItem = await _uow.Items.SaveItems(itemDto);

            return savedItem;
        }

        //done
      
        //Done
        public async Task<bool> SaveQuote(string QuoteJsonSTring)
        {
           var SaveQUoteResponse = await _uow.Items.SaveQuote(QuoteJsonSTring);

            return SaveQUoteResponse;
        }
        
        public async Task<QuoteDto> SendQuote(SendQuoteRequestModel sendQuoteRequestModel)
        {
          var quoteObj= await  _uow.Items.GetQuoteById(sendQuoteRequestModel.QuoteId);
            string body = null;

            List<ItemDto> deserializedProduct = JsonConvert.DeserializeObject<List<ItemDto>>(quoteObj.QuoteJsonString);
         // body = body.Replace("REFCODE", bookingResponse.RefCode).Replace("FULLNAME", bookingResponse.CustName).Replace("EmployeeName", userData.firstname + " " + userData.lastname).Replace("EmployeeEmail", userData.Email).Replace("MobileNo", userData.MobileNo).Replace("EmployeePic", "data:image/jpeg;base64," + userPic);
         //   _userMailer.SendHtmlMail(sendQuoteRequestModel.Email, "Your PlanPaddy Quote",body);
            var returnQuote = new QuoteDto
            {
                QuoteId = quoteObj.QuoteId,
                QuoteJsonString = quoteObj.QuoteJsonString
            };
             return returnQuote;


        }

        public async Task<ItemConfigurationDto> CreateItemConfiguration(ItemConfigurationDto itemConfigurationDto)
        {
            var itemConfig = await _uow.Items.CreateItemConfiguration(itemConfigurationDto);
            return itemConfig;
        }



        //private bool disposedValue = false;
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (!disposedValue)
        //    {
        //        if (disposing)
        //        {
        //            _itemRepo.Dispose();

        //        }
        //        disposedValue = true;
        //    }
        //}
        //public void Dispose()
        //{
        //    Dispose(true);
        //}


        public async Task<List<ItemDto>> GetItemsConfigurationByCategoryId(Guid CategoryId)
        {
            try
            {
                //trips.FirstOrDefault(t => t.ParentTripId == bus.TripId);
                 return await _uow.Items.GetItemsConfigurationByCategoryId(CategoryId);
                
            }
            catch (Exception e)
            {
                throw e;
            }


        }


        public async Task<ItemDVendorByPriceResList> GetQuoteByVendorPrice(ItemDVendorByPriceList itemDVendorByPrice)
        {
            ItemDVendorByPriceResList ItemList = new ItemDVendorByPriceResList();
            try
            {
                //trips.FirstOrDefault(t => t.ParentTripId == bus.TripId);
               //var ItemList = await _uow.Items.GetItemsConfigurationByCategoryId(itemDVendorByPrice.CategoryId);
                VendorItemRequestModel vendorItemRequestModel = new VendorItemRequestModel();

                foreach (var item in itemDVendorByPrice.Items)
                {
                    ItemDVendorByPriceRes itemDVendorByPriceRes = new ItemDVendorByPriceRes();
                    //item.LowerLimit = (Convert.ToDouble(item.ItemPercentage) * getQuoteRequestModel.LowerLimit) / 100;
                    //item.UpperLimit = (Convert.ToDouble(item.ItemPercentage) * getQuoteRequestModel.UpperLimit) / 100;
                    vendorItemRequestModel.ItemId = item.ItemId;
                    vendorItemRequestModel.LowerLimit = item.LowerLimit;
                    vendorItemRequestModel.UpperLimit = item.UpperLimit;
                    var VendorList = await _uow.Items.GetVendorItemConfigurationWithPrice(vendorItemRequestModel);
                    itemDVendorByPriceRes.ItemId = item.ItemId;
                    itemDVendorByPriceRes.LowerLimit = item.LowerLimit;
                    itemDVendorByPriceRes.UpperLimit = item.UpperLimit;
                    itemDVendorByPriceRes.Vendors = VendorList;
                    ItemList.ItemsWithVendors.Add(itemDVendorByPriceRes);
                }

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var ItemListString = serializer.Serialize(itemDVendorByPrice.Items);
                // = ItemList.ToString();
                var saveQuoteRes = await SaveQuote(ItemListString);
                if (saveQuoteRes == true)
                {
                    return ItemList;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw e;
            }


        }

    }
}
