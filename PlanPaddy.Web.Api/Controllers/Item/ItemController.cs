﻿using PlanPaddy.Core.DTOs;
using PlanPaddy.Core.Services.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace PlanPaddy.Web.Api.Controllers.Item
{
    [RoutePrefix("api/Item")]
    public class ItemController : ApiController
    {
        private readonly IItemService _service;

        public ItemController(IItemService service)
        {
            _service = service;
        }

        [HttpPost]
        [Route("GetQuote")]
        public async Task<List<ItemDto>> GetQuote(GetQuoteRequestModel getQuoteRequestModel)
        {


            var Quotes = await _service.GetQuote(getQuoteRequestModel);
            return Quotes;
        }
       

        [HttpPost]
        [Route("SendQuote")]
        public async Task<QuoteDto> SendQuote(SendQuoteRequestModel sendQuoteRequestModel)
        {
            var Quotes = await _service.SendQuote(sendQuoteRequestModel);
            return Quotes;
        }


       

        [HttpPost]
        [Route("PostItem")]
        public async Task<string> PostItem(ItemDto itemDto)
        {
            var itemDetails = await _service.PostItem(itemDto);
            return itemDetails;
        }
        [HttpPost]
        [Route("PostItemConfiguration")]
        public async Task<ItemConfigurationDto> PostItemConfiguration(ItemConfigurationDto itemConfigurationDto)
        {
            var itemConfigDetails = await _service.CreateItemConfiguration(itemConfigurationDto);
            return itemConfigDetails;
        }

        [HttpGet]
        [Route("GetAllItems")]
        public async Task<List<ItemDto>> GetAllItems()
        {


            var items = await _service.GetAllItems();
            return items;
        }


       

        [HttpGet]
        [Route("GetAllActiveCategories")]
        public async Task<GetCategoryResponseDto> GetAllActiveCategories()
        {
            var activeCategories = await _service.GetAllActiveCategories();
            return activeCategories;
        }

        [HttpGet]
        [Route("GetAllPriceRange")]
        public async Task<PriceRangeResDTO> GetAllPriceRange()
        {
            var priceRangeResDTO = await _service.GetAllPriceRange();
            return priceRangeResDTO;
        }

        [HttpGet]
        [Route("GetItemsConfigurationByCategoryId")]
        public async Task<List<ItemDto>> GetItemsConfigurationByCategoryId(Guid CategoryId)
        {


            var items = await _service.GetItemsConfigurationByCategoryId(CategoryId);
            return items;
        }

        [HttpPost]
        [Route("GetQuoteByVendorPrice")]
        public async Task<ItemDVendorByPriceResList> GetQuoteByVendorPrice(ItemDVendorByPriceList itemDVendorByPrice)
        {
            var Quotes = await _service.GetQuoteByVendorPrice(itemDVendorByPrice);
            return Quotes;
        }

    }
}
