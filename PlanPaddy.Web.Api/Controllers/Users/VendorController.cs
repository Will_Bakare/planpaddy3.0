﻿using PlanPaddy.Core.DTOs;
using PlanPaddy.Core.Services.Vendor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace PlanPaddy.Web.Api.Controllers.Users
{
    [RoutePrefix("api/Vendor")]
    public class VendorController : ApiController
    {
        private readonly IVendorService _service;

        public VendorController(IVendorService service)
        {
            _service = service;
        }

        [HttpPost]
        [Route("PostVendor")]
        public async Task<VendorDto> PostVendor(VendorPostRequest vendorDto)
        {
            var vendorDetails = await _service.PostVendor(vendorDto);
            return vendorDetails;
        }

        [HttpPost]
        [Route("ResendVerificationCode")]
        public async Task<VendorDto> ResendVerificationCode(VendorPostRequest vendorDto)
        {
            var vendorDetails = await _service.ResendVerificationCode(vendorDto);
            return vendorDetails;
        }

        [HttpPost]
        [Route("MocMail")]
        public async Task<VendorDto> MocMail(VendorDto vendor)
        {
            var vendorDetails = await _service.MocMail(vendor);
            return vendorDetails;
        }



        [HttpPost]
        [Route("PostVendorItems")]
        public async Task<VendorDto> PostVendorItems(VendorDto vendorDto)
        {
            var vendorDetails = await _service.PostVendorItems(vendorDto);
            return vendorDetails;
        }

        [HttpPost]
        [Route("SignIn")]
        public async Task<VendorDto> SignIn(VendorDto vendorDto)
        {
            var vendorDetails = await _service.SignIn(vendorDto);

            return vendorDetails;
        }

        [HttpPost]
        [Route("RequestResetPassword")]
        public async Task<VendorDto> RequestResetPassword(VendorDto vendorDto)
        {
            var vendorDetails = await _service.RequestResetPassword(vendorDto);

            return vendorDetails;
        }

        [HttpPost]
        [Route("ResetPassword")]
        public async Task<VendorDto> ResetPassword(VendorDto vendorDto)
        {
            var vendorDetails = await _service.ResetPassword(vendorDto);

            return vendorDetails;
        }


        [HttpPost]
        [Route("GetVendorDetailsByRefCode")]
        public async Task<VendorDto> GetVendorDetailsByRefCode(GetVendorDetailsRequestModel getVendorDetailsRequestModel)
        {
            var vendorDetails = await _service.GetVendorDetailsByRefCode(getVendorDetailsRequestModel.RefCode);
            return vendorDetails;
        }

        [HttpPost]
        [Route("GetAllVendors")]
        public async Task<List<VendorDto>> GetAllVendors()
        {
            var vendors = await _service.GetAllVendors();
            return vendors;
        }

        [HttpPost]
        [Route("GetVendorDetailsByEmail")]
        public async Task<VendorDto> GetVendorDetailsByEmail(VendorDto vendorDto)
        {
            var vendors = await _service.GetVendorDetailsByEmail(vendorDto.EmailAddress);
            return vendors;
        }

        [HttpPost]
        [Route("EditVendor")]
        public async Task<VendorDto> EditVendor(VendorDto vendorDto)
        {
            var vendor = await _service.EditVendor(vendorDto);
            return vendor;
        }

        [HttpPost]
        [Route("ConfirmActivationCode")]
        public async Task<VendorDto> ConfirmActivationCode(VendorDto vendorDto)
        {
            var vendor = await _service.ConfirmActivationCode(vendorDto);
            return vendor;
        }

        [HttpPost]
        [Route("UpdateVendorDetails")]
        public async Task<VendorDto> UpdateVendorDetails(VendorDto vendorDto)
        {
            var vendor = await _service.UpdateVendorDetails(vendorDto);
            return vendor;
        }
    }
}