﻿using PlanPaddy.Core.DTOs;
using PlanPaddy.Core.Services.Host;
using PlanPaddy.Core.Services.Vendor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace PlanPaddy.Web.Api.Controllers.Hosts
{
    [RoutePrefix("api/Host")]
    public class HostController : ApiController
    {
        // GET: Host
        private readonly IHostService _service;

        public HostController(IHostService service)
        {
            _service = service;
        }

        [HttpPost]
        [Route("SaveHost")]
        public async Task<HostDto> PostHost(HostDto hostDto)
        {
            var hostDetails = await _service.SaveHost(hostDto);
            return hostDetails;
        }

        [HttpGet]
        [Route("getHost")]
        public async Task<HostDto> PostHost(string Refcode)
        {
            var hostDetails = await _service.GetHost(Refcode);
            return hostDetails;
        }
    }
}