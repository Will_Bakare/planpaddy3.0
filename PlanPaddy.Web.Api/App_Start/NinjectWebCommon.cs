[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(PlanPaddy.Web.Api.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(PlanPaddy.Web.Api.App_Start.NinjectWebCommon), "Stop")]

namespace PlanPaddy.Web.Api.App_Start
{
    using System;
    using System.Web;
    using System.Web.Http;
    using Microsoft.AspNet.Identity;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Common.WebHost;
    using PlanPaddy.Core.Persistence;
    using PlanPaddy.Core.Persistence.Repository.Hosts;
    using PlanPaddy.Core.Persistence.Repository.Items;
    using PlanPaddy.Core.Persistence.Repository.Vendors;
    using PlanPaddy.Core.Services.Host;
    using PlanPaddy.Core.Services.Item;
    using PlanPaddy.Core.Services.Vendor;
    using PlanPaddy.Messaging.Email;
    using PlanPaddy.Repository;
    using PlanPaddy.Repository.Implementation;
    using PlanPaddy.Repository.Implementation.Hosts;
    using PlanPaddy.Repository.Implementation.Items;
    using PlanPaddy.Repository.Vendors;
    using PlanPaddy.Services.Host;
    using PlanPaddy.Services.Item;
    using PlanPaddy.Services.Security;
    using PlanPaddy.Services.Vendor;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                GlobalConfiguration.Configuration.DependencyResolver = new Ninject.WebApi.DependencyResolver.NinjectDependencyResolver(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IUnitOfWork>().To<UnitOfWork<PlanPaddyDataContext>>();
            kernel.Bind<IItemRepository>().To<ItemRepository>();
            kernel.Bind<IItemService>().To<ItemService>();
            kernel.Bind<IVendorService>().To<VendorService>();
            kernel.Bind<IVendorRepository>().To<VendorRepository>();
            kernel.Bind<IUserMailer>().To<UserMailer>();
            kernel.Bind<IPasswordHasher>().To<PlanPaddyHasher>();
            kernel.Bind<IHostRepository>().To<HostRepository>();
            kernel.Bind<IHostService>().To<HostService>();
            //
        }
    }
}
