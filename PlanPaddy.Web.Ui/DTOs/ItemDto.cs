﻿using PlanPaddy.Core.Common.Enums;
using PlanPaddy.Core.Entities.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Web.Ui.DTOs
{
    public class ItemDto
    {
        public Guid ItemId { get; set; }
        public Guid CategoryId { get; set; }
        public double? ItemPercentage { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ItemType Type { get; set; }
        public double LowerLimit { get; set; }

        public double UpperLimit { get; set; }
        public List<VendorDto> Vendors { get; set; }
        public bool IsActive { get; set; }
    }
}
