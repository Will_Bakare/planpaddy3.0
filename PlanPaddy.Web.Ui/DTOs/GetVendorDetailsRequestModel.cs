﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Web.Ui.DTOs
{
   public class GetVendorDetailsRequestModel
    {
        public string RefCode { get; set; }
    }
}
