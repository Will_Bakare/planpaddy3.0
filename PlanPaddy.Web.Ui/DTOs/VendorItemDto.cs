﻿using PlanPaddy.Core.Entities.Item;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Web.Ui.DTOs
{
   public class VendorItemDto
    {
        public Guid VendorItemId { get; set; }

        public Guid ItemId { get; set; }
        public string Name { get; set; }
        public Guid VendorId { get; set; }
        public string EmailAddress { get; set; }
        public Guid CategoryId { get; set; }
        public double LowerLimit { get; set; }
        public int? Quantity { get; set; }
        public double UpperLimit { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public string LowerLimitString { get; set; }
        public string UpperLimitString { get; set; }
        ////FK Constraints
        //[ForeignKey("ItemId")]
        //public Item Item { get; set; }
        //[ForeignKey("CategoryId")]
        //public Category Category { get; set; }
        //[ForeignKey("VendorId")]
        //public Vendor Vendor { get; set; }
    }
}
