﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PlanPaddy.Web.Ui.Startup))]
namespace PlanPaddy.Web.Ui
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
