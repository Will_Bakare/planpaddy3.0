﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PlanPaddy.Web.Ui.Helpers
{
    public class PlanPaddyUrls
    {
        static string BaseUrl => ConfigurationManager.AppSettings["BaseUrl"];

        public static string GetQuote = BaseUrl + "api/Item/GetQuote";
        public static string PostVendor = BaseUrl + "api/Vendor/PostVendor";
        public static string VerifyVendor = BaseUrl + "api/Vendor/ConfirmActivationCode";
        public static string GetAllItems = BaseUrl + "api/Item/GetAllItems";
        public static string PostVendorItems = BaseUrl + "api/Vendor/PostVendorItems";
        public static string SignIn = BaseUrl + "api/Vendor/SignIn"; 
        public static string GetVendorDetailsByEmail = BaseUrl + "api/Vendor/GetVendorDetailsByEmail";
        public static string UpdateVendorDetails = BaseUrl + "api/Vendor/UpdateVendorDetails";
        public static string RequestResetPassword = BaseUrl + "api/Vendor/RequestResetPassword";
        public static string ResetPassword = BaseUrl + "api/Vendor/ResetPassword"; 
        public static string ResendVerification = BaseUrl + "api/Vendor/ResendVerificationCode";
    }
}