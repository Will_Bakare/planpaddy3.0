﻿//using PlanPaddy.Web.Ui.Helpers;
//using RestSharp;

using PlanPaddy.Web.Ui.DTOs;
using PlanPaddy.Web.Ui.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Script.Serialization;

namespace PlanPaddy.Web.Ui.Helpers
{
    public class Logical
    {
        private readonly HttpClientHelper client;

        public Logical()
        {
        }
        public static string ConvertAmount(double amount)
        {
            var new_amount = $"{amount:N}";
            return new_amount;
        }

        // Session["departureTerminalId"]

        public static async Task<List<ItemDto>> GetQuotes(GetQuoteRequestModel getQuoteRequestModel)
        {
            using (HttpClientHelper client = new HttpClientHelper())
            {
                List<ItemDto> ItemObj = new List<ItemDto>();
                // var pickUpObj = new PickUpTerminal();
                ErrorModel errorModel = null;

                await client.ProcessClientRequestAsync<List<ItemDto>>(PlanPaddyUrls.GetQuote, HttpMethod.Post, getQuoteRequestModel,
                success =>
                {


                    ItemObj = success;
                },
                    error =>
                    {
                        errorModel = error;
                    }
                );
                return ItemObj;
            }
        }

        public static async Task<VendorDto> PostVendor(VendorPostRequestModel postVendorModel)
        {
            using (HttpClientHelper client = new HttpClientHelper())
            {
                VendorDto ItemObj = new VendorDto();
                // var pickUpObj = new PickUpTerminal();
                ErrorModel errorModel = null;

                await client.ProcessClientRequestAsync<VendorDto>(PlanPaddyUrls.PostVendor, HttpMethod.Post, postVendorModel,
                success =>
                {


                    ItemObj = success;
                },
                    error =>
                    {
                        errorModel = error;
                    }
                );
                return ItemObj;
            }
        }

        public static async Task<VendorDto> ResendVerification(VendorPostRequestModel postVendorModel)
        {
            using (HttpClientHelper client = new HttpClientHelper())
            {
                VendorDto ItemObj = new VendorDto();
                // var pickUpObj = new PickUpTerminal();
                ErrorModel errorModel = null;

                await client.ProcessClientRequestAsync<VendorDto>(PlanPaddyUrls.ResendVerification, HttpMethod.Post, postVendorModel,
                success =>
                {


                    ItemObj = success;
                },
                    error =>
                    {
                        errorModel = error;
                    }
                );
                return ItemObj;
            }
        }
        

        public static async Task<VendorDto> UpdateVendorDetails(VendorPostRequestModel postVendorModel)
        {
            using (HttpClientHelper client = new HttpClientHelper())
            {
                VendorDto ItemObj = new VendorDto();
                // var pickUpObj = new PickUpTerminal();
                ErrorModel errorModel = null;

                await client.ProcessClientRequestAsync<VendorDto>(PlanPaddyUrls.UpdateVendorDetails, HttpMethod.Post, postVendorModel,
                success =>
                {


                    ItemObj = success;
                },
                    error =>
                    {
                        errorModel = error;
                    }
                );
                return ItemObj;
            }
        }


        public static async Task<VendorDto> VerifyVendor(VendorPostRequestModel postVendorModel)
        {
            using (HttpClientHelper client = new HttpClientHelper())
            {
                VendorDto ItemObj = new VendorDto();
                // var pickUpObj = new PickUpTerminal();
                ErrorModel errorModel = null;

                await client.ProcessClientRequestAsync<VendorDto>(PlanPaddyUrls.VerifyVendor, HttpMethod.Post, postVendorModel,
                success =>
                {


                    ItemObj = success;
                },
                    error =>
                    {
                        errorModel = error;
                    }
                );
                return ItemObj;
            }
        }

        public static async Task<List<ItemDto>> GetAllItems(GetQuoteRequestModel getQuoteRequestModel)
        {
            using (HttpClientHelper client = new HttpClientHelper())
            {
                List<ItemDto> ItemObj = new List<ItemDto>();
                // var pickUpObj = new PickUpTerminal();
                ErrorModel errorModel = null;

                await client.ProcessClientRequestAsync<List<ItemDto>>(PlanPaddyUrls.GetAllItems, HttpMethod.Post, getQuoteRequestModel,
                success =>
                {


                    ItemObj = success;
                },
                    error =>
                    {
                        errorModel = error;
                    }
                );
                return ItemObj;
            }
        }

        public static async Task<VendorDto> PostVendorItems(VendorDto vendorItemDto)
        {
            using (HttpClientHelper client = new HttpClientHelper())
            {
                VendorDto ItemObj = new VendorDto();
                // var pickUpObj = new PickUpTerminal();
                ErrorModel errorModel = null;

                await client.ProcessClientRequestAsync<VendorDto>(PlanPaddyUrls.PostVendorItems, HttpMethod.Post, vendorItemDto,
                success =>
                {
                    ItemObj = success;
                },
                    error =>
                    {
                        errorModel = error;
                    }
                );
                return ItemObj;
            }
        }

        public static async Task<VendorDto> SignIn(SignInModel signInModel)
        {
            using (HttpClientHelper client = new HttpClientHelper())
            {
                VendorDto ItemObj = new VendorDto();
                // var pickUpObj = new PickUpTerminal();
                ErrorModel errorModel = null;

                await client.ProcessClientRequestAsync<VendorDto>(PlanPaddyUrls.SignIn, HttpMethod.Post, signInModel,
                success =>
                {
                    ItemObj = success;
                },
                    error =>
                    {
                        errorModel = error;
                    }
                );
                return ItemObj;
            }
        }

        public static async Task<VendorDto> RequestResetPassword(SignInModel signInModel)
        {
            using (HttpClientHelper client = new HttpClientHelper())
            {
                VendorDto ItemObj = new VendorDto();
                // var pickUpObj = new PickUpTerminal();
                ErrorModel errorModel = null;

                await client.ProcessClientRequestAsync<VendorDto>(PlanPaddyUrls.RequestResetPassword, HttpMethod.Post, signInModel,
                success =>
                {
                    ItemObj = success;
                },
                    error =>
                    {
                        errorModel = error;
                    }
                );
                return ItemObj;
            }
        }

        public static async Task<VendorDto> ResetPassword(VendorPostRequestModel vendorPostRequestModel)
        {
            using (HttpClientHelper client = new HttpClientHelper())
            {
                VendorDto ItemObj = new VendorDto();
                // var pickUpObj = new PickUpTerminal();
                ErrorModel errorModel = null;

                await client.ProcessClientRequestAsync<VendorDto>(PlanPaddyUrls.ResetPassword, HttpMethod.Post, vendorPostRequestModel,
                success =>
                {
                    ItemObj = success;
                },
                    error =>
                    {
                        errorModel = error;
                    }
                );
                return ItemObj;
            }
        }

        public static async Task<VendorDto> GetVendorDetailsByEmail(SignInModel signInModel)
        {
            using (HttpClientHelper client = new HttpClientHelper())
            {
                VendorDto ItemObj = new VendorDto();
                // var pickUpObj = new PickUpTerminal();
                ErrorModel errorModel = null;

                await client.ProcessClientRequestAsync<VendorDto>(PlanPaddyUrls.GetVendorDetailsByEmail, HttpMethod.Post, signInModel,
                success =>
                {
                    ItemObj = success;
                },
                    error =>
                    {
                        errorModel = error;
                    }
                );
                return ItemObj;
            }
        }

        //public static async Task<TicketBookingDto> GetBookingRefDetailsForReschedule(TicketBookingRequest ticketBookingDto)
        //{
        //    using (HttpClientHelper client = new HttpClientHelper())
        //    {
        //        TicketBookingDto bookingDetails = new TicketBookingDto();
        //        await client.ProcessClientRequestAsync<TicketBookingDto>(GigUrl.GetBookingRefDetailsForReschedule, HttpMethod.Post, ticketBookingDto,
        //            success =>
        //            {
        //                if (success != null)
        //                {
        //                    bookingDetails = success;
        //                }
        //                else
        //                {

        //                }

        //            },
        //                error =>
        //                {
        //                    bookingDetails.Object = null;
        //                }
        //            );
        //        return bookingDetails;
        //    }
        //}

        //public static async Task<GetRemainingSeat> Getavailableseats(string vehicletripregistrationId)
        //{
        //    using (HttpClientHelper client = new HttpClientHelper())
        //    {
        //        GetRemainingSeat bookingDetails = new GetRemainingSeat();
        //        await client.ProcessClientRequestAsync<GetRemainingSeat>(GigUrl.Getavailableseats + vehicletripregistrationId, HttpMethod.Get, vehicletripregistrationId,
        //            success =>
        //            {
        //                if (success != null)
        //                {
        //                    bookingDetails = success;
        //                }
        //                else
        //                {

        //                }

        //            },
        //                error =>
        //                {
        //                    bookingDetails.Object = null;
        //                }
        //            );
        //        return bookingDetails;
        //    }
        //}
        //public static async Task<CouponResponse> GetCouponValidity(string couponCode)
        //{
        //    using (HttpClientHelper client = new HttpClientHelper())
        //    {
        //        CouponResponse couponObject = new CouponResponse();
        //        ErrorModel errorModel = null;

        //        await client.ProcessClientRequestAsync<CouponResponse>(GigUrl.GetCouponValidity + couponCode, HttpMethod.Get, "CouponCode",
        //        success =>
        //        {
        //            couponObject = success;
        //        },
        //            error =>
        //            {
        //                errorModel = error;
        //            }
        //        );
        //        return couponObject;
        //    }
        //}

        //public static async Task<Tuple<getHireRefcodeResponse, ErrorModel>> GetHireRefcode(GetHireRefcodeRequest getHireRefcodeRequest)
        //{
        //    using (HttpClientHelper client = new HttpClientHelper())
        //    {
        //        getHireRefcodeResponse hireDetails = new getHireRefcodeResponse();
        //        ErrorModel errorModel = null;

        //        await client.ProcessClientRequestAsync<getHireRefcodeResponse>(GigUrl.GetHireRefcodeBuses, HttpMethod.Post, getHireRefcodeRequest,
        //            success =>
        //            {
        //                if (success != null)
        //                {
        //                    hireDetails = success;
        //                }
        //                else
        //                {

        //                }

        //            },
        //                error =>
        //                {
        //                    errorModel = error;
        //                }
        //            );
        //        return new Tuple<getHireRefcodeResponse, ErrorModel>(hireDetails, errorModel);

        //    }
        //}
    }
}
