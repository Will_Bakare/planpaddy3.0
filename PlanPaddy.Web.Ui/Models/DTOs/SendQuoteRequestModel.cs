﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Web.Ui.DTOs
{
   public class SendQuoteRequestModel
    {
        public string Email { get; set; }
        public Guid QuoteId { get; set; }
    }
}
