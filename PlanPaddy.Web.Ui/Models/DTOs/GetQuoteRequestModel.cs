﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Web.Ui.DTOs
{
    public class GetQuoteRequestModel
    {
        public string CategoryId { get; set; }
        public string LowerLimit { get; set; }
        public string UpperLimit { get; set; }

    }
}
