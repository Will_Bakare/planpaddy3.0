﻿using PlanPaddy.Core.Entities.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Web.Ui.DTOs
{
    public class VendorDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public string Picture { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNo { get; set; }
        public string RefCode { get; set; }
        public bool IsActive { get; set; }

        public List<VendorItemDto> VendorItems { get; set; }

    }
}
