﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Web.Ui.DTOs
{
    public class VendorItemRequestModel
    {
        public Guid ItemId { get; set; }
        public double LowerLimit { get; set; }
        public double UpperLimit { get; set; }
    }
}
