﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace PlanPaddy.Web.Ui.Models.Enums
{
    public enum Budget
    {
        [Description("1,000,000")]
        OneMillion,
        [Description("1,500,000")]
        OnePointFiveMillion,
        [Description("2,000,000")]
        TwoMillion,
        [Description("2,500,000")]
        TwoPointFiveMillion,
        [Description("3,000,000")]
        ThreeMillion,
        [Description("3,500,000")]
        ThreePointFiveMillion,
        [Description("4,000,000")]
        FourMillion,
        [Description("4,500,000")]
        FourPointFiveMillion,
        [Description("5,000,000")]
        FiveMillion


    }
}