﻿$(document).ready(function () {
    //============================ Number Mask ==============================
    $('#LowerLimit').mask('000,000,000,000,000', { reverse: true });
    $('#UpperLimit').mask('000,000,000,000,000', { reverse: true });
});