﻿using PlanPaddy.Web.Ui.DTOs;
using PlanPaddy.Web.Ui.Helpers;
using PlanPaddy.Web.Ui.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PlanPaddy.Web.Ui.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var budgetList = new SelectList(new[] {
                new{ID="0", Name="Select a Budget Range"},
                new{ID="1000000", Name="₦1,000,000 => ₦1,500,000 "},
                 new{ID="2000000", Name="₦2,000,000 => ₦2,500,000"},
                     new{ID="3000000", Name="₦3,000,000 => ₦3,500,000"},
                     new{ID="4000000", Name="₦4,000,000 => ₦4,500,000"},
                     new{ID="5000000", Name="₦5,000,000 => ₦6,000,000"},
            },
            "ID", "Name", 1
            );

            var categoryList = new SelectList(new[] {
                new{ID="0", Name="Select a Category"},
                new{ID="2BFFB492-AA8C-4DE1-BB40-6CCF2BBB5B8A", Name="Wedding"},
            },
           "ID", "Name", 1
           );
            ViewData["CatList"] = categoryList;
            ViewData["BudList"] = budgetList;
            return View();
        }

        public ActionResult Welcome()
        {
            var budgetList = new SelectList(new[] {
                new{ID="0", Name="Select a Budget Range"},
                new{ID="1000000", Name="₦1,000,000 => ₦1,500,000 "},
                 new{ID="2000000", Name="₦2,000,000 => ₦2,500,000"},
                     new{ID="3000000", Name="₦3,000,000 => ₦3,500,000"},
                     new{ID="4000000", Name="₦4,000,000 => ₦4,500,000"},
                     new{ID="5000000", Name="₦5,000,000 => ₦6,000,000"},
            },
            "ID", "Name", 1
            );

            var categoryList = new SelectList(new[] {
                new{ID="0", Name="Select a Category"},
                new{ID="2BFFB492-AA8C-4DE1-BB40-6CCF2BBB5B8A", Name="Wedding"},
            },
           "ID", "Name", 1
           );
            ViewData["CatList"] = categoryList;
            ViewData["BudList"] = budgetList;
            return View("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Stories()
        {
            return Redirect("http://stories.planpaddy.com");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public async  Task<ActionResult> GetQuote(GetQuoteRequestModel getQuoteModel)
        {
            ViewBag.Message = ".";
            if (Convert.ToDouble(getQuoteModel.LowerLimit) ==0)
            {
               // string catDDLValue = Request.Form["CategoryId"].ToString();
                string limitDDLValue = Request.Form["LowerLimit"].ToString();
              //  var category = catDDLValue.Split(',');
                var lowerLimit = limitDDLValue.Split(',');
                //var getQuoteModel = new GetQuoteRequestModel();

                // getQuoteModel.CategoryId = category[1];

                if (lowerLimit.Length >1)
                {
                    getQuoteModel.LowerLimit = lowerLimit[1];
                }
                else
                {
                    getQuoteModel.LowerLimit = "1000000";
                }
              
            }

         

            getQuoteModel.CategoryId = "2BFFB492-AA8C-4DE1-BB40-6CCF2BBB5B8A";
            //getQuoteModel.UpperLimit = Convert.ToDouble("");
         
            getQuoteModel.UpperLimit = (Convert.ToDouble(getQuoteModel.LowerLimit) + 500000).ToString();

            var getQuoteResponse = await Logical.GetQuotes(getQuoteModel);

           // var budgetList = new SelectList(new[] {
           //     new{ID="0", Name="Select a Budget Range"},
           //     new{ID="1000000", Name="₦1,000,000 => ₦1,500,000 "},
           //      new{ID="2000000", Name="₦2,000,000 => ₦2,500,000"},
           //          new{ID="3000000", Name="₦3,000,000 => ₦3,500,000"},
           //          new{ID="4000000", Name="₦4,000,000 => ₦4,500,000"},
           //          new{ID="5000000", Name="₦5,000,000 => ₦6,000,000"},
           // },
           //"ID", "Name", 1
           //);

           // var categoryList = new SelectList(new[] {
           //     new{ID="0", Name="Select a Category"},
           //     new{ID="2BFFB492-AA8C-4DE1-BB40-6CCF2BBB5B8A", Name="Wedding"},
           // },
           //"ID", "Name", 1
           //);
            //ViewData["CatList"] = categoryList;
            //ViewData["BudList"] = budgetList;
            ViewData["searchResult"] =getQuoteResponse;
            return View("QuoteList");
        }

        public ActionResult Login()
        {
            ViewBag.Message = "Login page.";

            return View();
        }

        public ActionResult ForgotPassword()
        {
            ViewBag.Message = "Forgot Password page.";

            return View();
        }

        public async Task<ActionResult> VendorProfile(SignInModel signInModel)
        {
            var verifyVendorResponse =await  Logical.GetVendorDetailsByEmail(signInModel);
            ViewData["VendorDetails"] = verifyVendorResponse;
            return View();
        }

        public async Task<ActionResult> SignIn(SignInModel signInModel)
        {
            var vendorDetails = await Logical.SignIn(signInModel);
            ViewData["VendorDetails"] = vendorDetails;
            if (vendorDetails.IsTempPassword)
            {
                ViewData["VendorEmail"] = vendorDetails.EmailAddress;
                return View("ResetPassword");
            }
            else
            {
                if (vendorDetails.FirstName != null && vendorDetails.AccountIsConfirmed == true)
                {
                    return View("VendorProfile");
                }
                else
                {
                    ViewData["Error"] = "Invalid Login Details. Please try again";
                    return View("Login");
                }
            }
           
            
        }

        public async Task<ActionResult> RequestResetPassword(SignInModel signInModel)
        {
            var vendorDetails = await Logical.RequestResetPassword(signInModel);
            ViewData["VendorDetails"] = vendorDetails;
         
                if (vendorDetails.FirstName != null)
                {
                    ViewData["ResetResponse"] = "Thank you. We have sent you an email. Kindly login with the temporary password sent to you";
                    return View("Login");
                }
                else
                {
                    ViewData["Error"] = "We were unable to process your request. Please try again";
                    return View("Login");
                }
            


        }

        public async Task<ActionResult> ResetPassword(VendorPostRequestModel vendorPostRequestModel)
        {
            var vendorDetails = await Logical.ResetPassword(vendorPostRequestModel);
            ViewData["VendorDetails"] = vendorDetails;
            if (vendorDetails.IsTempPassword)
            {
                ViewData["VendorEmail"] = vendorDetails.EmailAddress;
                ViewData["Error"] = "We were unable to process your request. Please try again";
                return View("ResetPassword");
            }
            else
            {
                if (vendorDetails.FirstName != null && vendorDetails.AccountIsConfirmed == true)
                {
                    return View("VendorProfile");
                }
                else
                {
                    ViewData["Error"] = "We were unable to process your request. Please try again";
                    return View("Login");
                }
            }

        }

        public ActionResult SignUp()
        {
            ViewBag.Message = "SignUp page.";

            return View();
        }

        public async Task<ActionResult> PostVendor(VendorPostRequestModel postVendorModel)
        {
            var postVendorResponse = await Logical.PostVendor(postVendorModel);
            ViewData["VendorEmail"] = postVendorResponse.EmailAddress;

            #region do not verify
         
            var categoryList = new SelectList(new[] {
                new{ID="0", Name="Select a Category"},
                new{ID="2BFFB492-AA8C-4DE1-BB40-6CCF2BBB5B8A", Name="Wedding"},
            },
                      "ID", "Name", 1
                                              );
            var postObj = new GetQuoteRequestModel();
            postObj.CategoryId = "2BFFB492-AA8C-4DE1-BB40-6CCF2BBB5B8A";


            var getItemsResponse = await Logical.GetAllItems(postObj);
            var itemList = new SelectList(getItemsResponse, "ItemId", "Name", 1);
            ViewData["CatList"] = categoryList;
            ViewData["ItemList"] = itemList;
            ViewData["VendorEmail"] = postVendorResponse.EmailAddress;

            if (postVendorResponse.FirstName != null)
            {
                return RedirectToAction("VendorItems", "Home", new { EmailAddress = postVendorResponse.EmailAddress });
            }
            #endregion




            #region verify vendor
            //if (postVendorResponse.FirstName!= null)
            //{
            //    return View("Verify");
            //}
            #endregion
            else
            {
                ViewData["Error"] = "We were unable to process your request. Please try again";
                return View("SignUp");
            }
           
        }


        public async Task<ActionResult> ResendVerification(VendorPostRequestModel postVendorModel)
        {
            var postVendorResponse = await Logical.ResendVerification(postVendorModel);
            ViewData["VendorEmail"] = postVendorResponse.EmailAddress;
           
            if (postVendorResponse.FirstName != null)
            {
                ViewData["ResendVerificationResponse"] = "Verification Code has been sent to you again";
                return View("Verify");
            }
            else
            {
                ViewData["Error"] = "We were unable to process your request. Please try again";
                return View("Verify");
            }

        }
        public async Task<ActionResult> UpdateVendor(VendorPostRequestModel postVendorModel)
        {
            var postVendorResponse = await Logical.UpdateVendorDetails(postVendorModel);
            ViewData["VendorDetails"] = postVendorResponse;
            return View("VendorProfile");
        }
        public async Task<ActionResult> Verify(VendorPostRequestModel postVendorModel)
        {

            var verifyVendorResponse = await Logical.VerifyVendor(postVendorModel);

            var categoryList = new SelectList(new[] {
                new{ID="0", Name="Select a Category"},
                new{ID="2BFFB492-AA8C-4DE1-BB40-6CCF2BBB5B8A", Name="Wedding"},
            },
         "ID", "Name", 1
         );
            var postObj = new GetQuoteRequestModel();
            postObj.CategoryId = "2BFFB492-AA8C-4DE1-BB40-6CCF2BBB5B8A";
            

            var getItemsResponse = await Logical.GetAllItems(postObj);
            var itemList = new SelectList(getItemsResponse,"ItemId", "Name", 1);
            ViewData["CatList"] = categoryList;
            ViewData["ItemList"] = itemList;
            ViewData["VendorEmail"] = verifyVendorResponse.EmailAddress;
     
            if (verifyVendorResponse.AccountIsConfirmed)
            {
                return RedirectToAction("VendorItems", "Home",new { EmailAddress =verifyVendorResponse.EmailAddress });
            }
            else
            {
                ViewData["VendorEmail"] = verifyVendorResponse.EmailAddress;
                ViewData["Error"] = "Invalid Verification details. Please try again";
                return View("Verify");
            }
           
        }

        public async Task<ActionResult> VendorItems(VendorDto vendorDto)
        {
            SignInModel signInModel = new SignInModel();
            signInModel.EmailAddress = vendorDto.EmailAddress;
        var verifyVendorResponse =await  Logical.GetVendorDetailsByEmail(signInModel);
            vendorDto = verifyVendorResponse;
            var categoryList = new SelectList(new[] {
                new{ID="0", Name="Select a Category"},
                new{ID="2BFFB492-AA8C-4DE1-BB40-6CCF2BBB5B8A", Name="Wedding"},
            },
        "ID", "Name", 1
        );
            var postObj = new GetQuoteRequestModel();
            postObj.CategoryId = "2BFFB492-AA8C-4DE1-BB40-6CCF2BBB5B8A";


            var getItemsResponse = await Logical.GetAllItems(postObj);
           
            var itemList = new SelectList(getItemsResponse, "ItemId", "Name", 1);
            ViewData["CatList"] = categoryList;
            ViewData["ItemList"] = itemList;
            ViewData["VendorEmail"] = vendorDto.EmailAddress;
            ViewData["VendorDetails"] = vendorDto;

            if (vendorDto.AccountIsConfirmed)
            {
                ViewData["Response"] = TempData["Response"] as string;
                ViewData["Error"] = TempData["Error"] as string;
                return View();
            }
            ViewData["Error"] = "We were unable to process your request, please try again";
            return View();

            //  return View();
        }

        public async Task<ActionResult> PostVendorItems(VendorItemDto vendorItemDto)
        {
            var vendor = new VendorDto();
            var vendorItemList = new List<VendorItemDto>();
            vendorItemDto.CategoryId=new Guid("2BFFB492-AA8C-4DE1-BB40-6CCF2BBB5B8A");

            var lowAmount = vendorItemDto.LowerLimitString.Replace("₦", "");
            lowAmount= lowAmount.Replace(",", ""); 
             var maxAmount = vendorItemDto.UpperLimitString.Replace("₦", "");
            maxAmount = maxAmount.Replace(",", "");

            vendorItemDto.LowerLimit = Convert.ToDouble(lowAmount);
            vendorItemDto.UpperLimit = Convert.ToDouble(maxAmount);
            vendorItemList.Add(vendorItemDto);
            vendor.EmailAddress = vendorItemDto.EmailAddress;
            vendor.VendorItems = vendorItemList;


            var postVendorItemResponse = await Logical.PostVendorItems(vendor);

            if (postVendorItemResponse !=null)
            {
                if (postVendorItemResponse.VendorItems.Count() > 0 && postVendorItemResponse.FirstName != null)
                {
                    TempData["Response"] = "Update Successful!!! You may add more items/services if you provide such.";
                    return RedirectToAction("VendorItems", "Home", postVendorItemResponse);
                }
                else
                {
                    TempData["Error"] = "We encountered an error while saving your details. Please try again";
                    return RedirectToAction("VendorItems", "Home", postVendorItemResponse);
                }
            }
            else
            {
                TempData["Error"] = "We encountered an error while saving your details. Please try again";
                return RedirectToAction("VendorItems", "Home", postVendorItemResponse);
            }
          
            
        }
    }
}