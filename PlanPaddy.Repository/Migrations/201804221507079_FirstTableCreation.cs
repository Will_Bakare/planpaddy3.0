namespace PlanPaddy.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstTableCreation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        AddedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.ItemCategories",
                c => new
                    {
                        ItemCategoryId = c.Guid(nullable: false),
                        ItemId = c.Guid(nullable: false),
                        CategoryId = c.Guid(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        AddedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ItemCategoryId)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Type = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        AddedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "dbo.ItemConfigurations",
                c => new
                    {
                        ItemConfigurationId = c.Guid(nullable: false),
                        CategoryId = c.Guid(nullable: false),
                        ItemId = c.Guid(nullable: false),
                        ItemPercentage = c.Double(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        AddedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ItemConfigurationId)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "dbo.PriceRanges",
                c => new
                    {
                        PriceRangeId = c.Guid(nullable: false),
                        LowerLimit = c.Double(nullable: false),
                        UpperLimit = c.Double(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        AddedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PriceRangeId);
            
            CreateTable(
                "dbo.VendorItems",
                c => new
                    {
                        VendorItemId = c.Guid(nullable: false),
                        ItemId = c.Guid(nullable: false),
                        VendorId = c.Guid(nullable: false),
                        CategoryId = c.Guid(nullable: false),
                        LowerLimit = c.Double(nullable: false),
                        Quantity = c.Int(),
                        UpperLimit = c.Double(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        AddedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.VendorItemId)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("dbo.Vendors", t => t.VendorId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.VendorId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Vendors",
                c => new
                    {
                        VendorId = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Picture = c.String(),
                        EmailAddress = c.String(),
                        MobileNo = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        AddedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.VendorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VendorItems", "VendorId", "dbo.Vendors");
            DropForeignKey("dbo.VendorItems", "ItemId", "dbo.Items");
            DropForeignKey("dbo.VendorItems", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.ItemConfigurations", "ItemId", "dbo.Items");
            DropForeignKey("dbo.ItemConfigurations", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.ItemCategories", "ItemId", "dbo.Items");
            DropForeignKey("dbo.ItemCategories", "CategoryId", "dbo.Categories");
            DropIndex("dbo.VendorItems", new[] { "CategoryId" });
            DropIndex("dbo.VendorItems", new[] { "VendorId" });
            DropIndex("dbo.VendorItems", new[] { "ItemId" });
            DropIndex("dbo.ItemConfigurations", new[] { "ItemId" });
            DropIndex("dbo.ItemConfigurations", new[] { "CategoryId" });
            DropIndex("dbo.ItemCategories", new[] { "CategoryId" });
            DropIndex("dbo.ItemCategories", new[] { "ItemId" });
            DropTable("dbo.Vendors");
            DropTable("dbo.VendorItems");
            DropTable("dbo.PriceRanges");
            DropTable("dbo.ItemConfigurations");
            DropTable("dbo.Items");
            DropTable("dbo.ItemCategories");
            DropTable("dbo.Categories");
        }
    }
}
