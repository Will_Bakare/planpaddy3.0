﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using PlanPaddy.Core.Entities;
using System.Data.Entity.ModelConfiguration.Conventions;
using PlanPaddy.Core.Entities.Item;
using PlanPaddy.Core.Entities.Host;

namespace PlanPaddy.Repository
{
    public class PlanPaddyDataContext : DbContext
    {
        public PlanPaddyDataContext() : base("PlanPaddyDataContext")
        {
          Database.SetInitializer(new MigrateDatabaseToLatestVersion<PlanPaddyDataContext, Migrations.Configuration>());
        }

 

        public DbSet<Item> Items { get; set; }
        public DbSet<ItemConfiguration> ItemConfigurations { get; set; }
        public DbSet<ItemCategory> ItemCategories { get; set; }
        public DbSet<PriceRange> PriceRanges { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<VendorItem> VendorItems { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Quote> Quotes { get; set; }
        public DbSet<Host> Hosts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
         
        }


    }
}
