﻿using PlanPaddy.Core.Persistence;
using PlanPaddy.Core.Persistence.Repository.Items;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlanPaddy.Repository;
using Ninject;
using PlanPaddy.Core.Persistence.Repository.Vendors;
using PlanPaddy.Core.Persistence.Repository.Hosts;

namespace PlanPaddy.Repository.Implementation
{
   public class UnitOfWork <T>: IUnitOfWork where T : PlanPaddyDataContext
    {
        [Inject]
        public IItemRepository Items { get; set; }

        [Inject]
        public IVendorRepository Vendors { get; set; }

        [Inject]
        public IHostRepository Hosts { get; set; }


        #region Base Setup

        private readonly T _context;


        public UnitOfWork(T context)
        {
            _context = context;
        }
        #endregion

        #region Base Implementations

        public async Task CompleteAsync()
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    await _context.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (DbEntityValidationException devex)
                {
                    var outputLines = new StringBuilder();

                    foreach (var eve in devex.EntityValidationErrors)
                    {
                        outputLines.AppendLine(
                            $"{DateTime.Now}: Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors: ");

                        foreach (var ve in eve.ValidationErrors)
                        {
                            outputLines.AppendLine($"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"");
                        }
                    }

                    transaction.Rollback();

                    throw new DbEntityValidationException(outputLines.ToString(), devex);
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private volatile bool _isDisposing;

        private void Dispose(bool disposing)
        {
            if (_isDisposing) return;

            _isDisposing = disposing;
            _context.Dispose();
            _isDisposing = false;
        }

        #endregion
    }
}
