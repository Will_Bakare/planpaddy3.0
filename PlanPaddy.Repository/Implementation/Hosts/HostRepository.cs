﻿using PlanPaddy.Core.DTOs;
using PlanPaddy.Core.Entities.Host;
using PlanPaddy.Core.Persistence.Repository.Hosts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Repository.Implementation.Hosts
{
    public class HostRepository : Repository<Host>, IHostRepository
    {
        public HostRepository(PlanPaddyDataContext context) : base(context)
        {
        }

        public async Task<Host> GetHost(string Refcode)
        {
            var HostObj = await context.Hosts.Where(r => r.RefCode == Refcode).FirstOrDefaultAsync();
            return HostObj;
        }

        public async Task<HostDto> SaveHost(HostDto hostDto)
        {
            var HostToSave = new Host
            {
                Name = hostDto.Name,
                FirstName = hostDto.FirstName,
                LastName = hostDto.LastName,
                About = hostDto.About,
                Picture = hostDto.Picture,
                EmailAddress = hostDto.EmailAddress,
                MobileNo = hostDto.MobileNo,
                Password = hostDto.Password,
                IsActive = true,
                OTP = "",
                //SubscriptionPlan = hostDto.SubscriptionPlan,
               // ExpiryDate = DateTime.Now.AddDays(365),
                Instagram = hostDto.Instagram,
                Facebook = hostDto.Facebook,
                Twitter = hostDto.Twitter,
                OfficeAddress = hostDto.OfficeAddress,
                Verified = hostDto.Verified,
                AddedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                AccountIsConfirmed = true

            };
            var refcode = await GetRefCode();
            refcode += "-HST";
            HostToSave.RefCode = refcode;
            context.Hosts.Add(HostToSave);
            await context.SaveChangesAsync();

            var savedHost = await GetHost(HostToSave.RefCode);
            var savedHostDto = new HostDto
            {
                HostId = hostDto.HostId,
                Name = savedHost.Name,
                About = savedHost.About,
                Picture = savedHost.Picture,
                EmailAddress = savedHost.EmailAddress,
                MobileNo = savedHost.MobileNo,
                IsActive = savedHost.IsActive,
                RefCode = savedHost.RefCode,
            };
            return savedHostDto;
        }


        public async Task<string> GetRefCode()
        {

            var queryString = @"declare @code varchar(6)
                                    declare @rounds int = 1
                                    set @code = left( newid(), 6)

                                    while exists ( select * from vendors where RefCode like  @code + '%' )
                                        begin
                                            set @code = left( newid(), 6)
                                            set @rounds= @rounds + 1
                                        end

                                        select @code ";

            var rawQuery = context.Database.SqlQuery<String>(queryString);

            var strCode = await rawQuery.ToListAsync();

            if (strCode != null && strCode.Any())
                return strCode.FirstOrDefault()?.ToUpper();

            return string.Empty;
        }

    }
}
