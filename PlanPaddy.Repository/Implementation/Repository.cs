﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using PlanPaddy.Core;
using PlanPaddy.Core.Entities;
using PlanPaddy.Core.Persistence;

namespace PlanPaddy.Repository.Implementation
{
    public class Repository<T> : IRepository<T> where T: BaseEntity
    {

        //private readonly ApplicationContext context;
        //private DbSet<T> entities;

        public readonly PlanPaddyDataContext context;
        public DbSet<T> entities;
        string errorMessage = string.Empty;

        public Repository(PlanPaddyDataContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }

        public void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Remove(entity);
            context.SaveChanges();
        }

        public T Get(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Add(entity);
            context.SaveChanges();
        }

        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.SaveChanges();
        }
    }
}
