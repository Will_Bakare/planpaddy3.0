﻿using PlanPaddy.Core.DTOs;
using PlanPaddy.Core.Entities.Item;
using PlanPaddy.Core.Persistence.Repository.Items;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace PlanPaddy.Repository.Implementation.Items
{
    public class ItemRepository : Repository<Item>, IItemRepository
    {
        public ItemRepository(PlanPaddyDataContext context) : base(context)
        {
        }

        public void Delete(Item entity)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Item Get(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Item> GetAll()
        {
            throw new NotImplementedException();
        }

        public async Task<List<Item>> GetAllItems()
        {
            var activeItems = context.Items.Where(r => r.IsActive == true);
            return activeItems.ToList();
        }
       

        public Task<List<ItemDto>> GetItemsByCategory(Guid categoryId)
        {
            //throw new NotImplementedException();
            var Items =
                 from itemCategory in context.ItemCategories
                 where itemCategory.CategoryId == categoryId

                 select new ItemDto
                 {
                     ItemId = itemCategory.ItemId,
                     CategoryId = itemCategory.CategoryId,
                 };

            return Items.ToListAsync();
        }

        public Task<List<ItemDto>> GetItemsConfigurationByCategoryId(Guid categoryId)
        {
            try
            {
                var Items =
                  from itemConfiguration in context.ItemConfigurations
                  join items in context.Items on itemConfiguration.ItemId equals items.ItemId
                  where itemConfiguration.CategoryId == categoryId

                  select new ItemDto
                  {
                      ItemId = itemConfiguration.ItemId,
                      CategoryId = itemConfiguration.CategoryId,
                      ItemPercentage = itemConfiguration.ItemPercentage,
                      Name = items.Name,
                      Description = items.Description,
                      ImageUrl = items.ImageUrl
                  };

                return Items.ToListAsync();
            }
            catch (Exception e)
            {
                return null;
            }

        }


      

        public Task<List<VendorDto>> GetVendorItemConfiguration(VendorItemRequestModel vendorItemRequestModel)
        {
            var Vendors =
                  from vendorItems in context.VendorItems
                  join vendors in context.Vendors on vendorItems.VendorId equals vendors.VendorId
                  where vendorItems.ItemId == vendorItemRequestModel.ItemId
                 // && vendorItems.LowerLimit == vendorItemRequestModel.LowerLimit
                 // && vendorItems.UpperLimit == vendorItemRequestModel.UpperLimit
                  && vendorItems.IsActive == true

                  select new VendorDto
                  {
                      Id = vendors.VendorId,
                      Name = vendors.Name,
                      EmailAddress = vendors.EmailAddress,
                      MobileNo = vendors.MobileNo,
                      RefCode= vendors.RefCode,
                      Instagram= vendors.Instagram,
                      OfficeAddress= vendors.OfficeAddress
                  };


            return Vendors.ToListAsync();
        }

        public void Insert(Item entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> SaveQuote(string quoteJsonSTring)
        {
            if (quoteJsonSTring != null)
            {
                try
                {
                    var QuoteToSave = new Quote
                    {
                        QuoteJsonString = quoteJsonSTring,
                        AddedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now
                    };
                    context.Quotes.Add(QuoteToSave);
                    await context.SaveChangesAsync();


                    return true;
                }
                catch (Exception e)
                {
                    //throw e;
                    return false;
                }
            }


            return false;
        }


        public async Task<Quote> GetQuoteById(Guid quoteId)
        {
            var Quote = await context.Quotes.Where(r => r.QuoteId == quoteId).FirstOrDefaultAsync();
            return Quote;
        }

        public async Task<Vendor> GetVendorByRefCode(string refCode)
        {
            var VendorObj = await context.Vendors.Where(r => r.RefCode == refCode).FirstOrDefaultAsync();
            return VendorObj;
        }
        public async Task<List<VendorItem>> GetVendorItemsByVendorId(Guid vendorId)
        {
            var VendorItemObj =  context.VendorItems.Where(r => r.VendorId == vendorId);

            return VendorItemObj.ToList();
        }

        public void Update(Item entity)
        {
            throw new NotImplementedException();
        }


        public async Task<ItemConfigurationDto> GetItemConfiguration(ItemConfigurationDto itemConfigurationDto)
        {
           var itemConfig =  await context.ItemConfigurations.Where(r => r.ItemId == itemConfigurationDto.ItemId && r.CategoryId == itemConfigurationDto.CategoryId).FirstOrDefaultAsync();
            var configToReturn = new ItemConfigurationDto
            {
                ItemConfigurationId = itemConfig.ItemConfigurationId,
                ItemId = itemConfig.ItemId,
                ItemPercentage = itemConfig.ItemPercentage,
                CategoryId =itemConfig.CategoryId,
                IsActive=itemConfig.IsActive
            };
            return configToReturn;
        }

        public async Task<ItemConfigurationDto> CreateItemConfiguration(ItemConfigurationDto itemConfigurationDto)
        {
            try
            {
                var ConfigToSave = new ItemConfiguration
                {
                    ItemId = itemConfigurationDto.ItemId,
                    CategoryId = itemConfigurationDto.CategoryId,
                    IsActive= itemConfigurationDto.IsActive,
                    ItemPercentage=itemConfigurationDto.ItemPercentage,
                    AddedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now
                };
                context.ItemConfigurations.Add(ConfigToSave);
                await context.SaveChangesAsync();


            }
            catch (Exception e)
            {
                return null;
            }
            var configFound = await GetItemConfiguration(itemConfigurationDto);
           
            return configFound;

        }
        public async Task<VendorDto> SaveVendor(VendorDto vendorDto)
        {
            var VendorToSave = new Vendor
            {
                Name = vendorDto.Name,
                Description = vendorDto.Description,
                Picture = vendorDto.Picture,
                EmailAddress = vendorDto.EmailAddress,
                MobileNo = vendorDto.MobileNo,
                IsActive = true

            };
            var refcode = await GetRefCode();
            refcode += "-VEN";
            VendorToSave.RefCode = refcode;
            context.Vendors.Add(VendorToSave);
            await context.SaveChangesAsync();

            var savedVendor = await GetVendorByRefCode(VendorToSave.RefCode);
            var savedVendorDto = new VendorDto
            {
                Id = VendorToSave.VendorId,
                Name = savedVendor.Name,
                Description = savedVendor.Description,
                Picture = savedVendor.Picture,
                EmailAddress = savedVendor.EmailAddress,
                MobileNo = savedVendor.MobileNo,
                IsActive = savedVendor.IsActive,
                RefCode = savedVendor.RefCode,
            };
            return savedVendorDto;
        }


        public async Task<string> SaveVendorItems(List<VendorItemDto> vendorItems)
        {
            foreach (var vendorItem in vendorItems)
            {
                var VendorItemToSave = new VendorItem
                {

                    ItemId = vendorItem.ItemId,
                    VendorId = vendorItem.VendorId,
                    CategoryId = vendorItem.CategoryId,
                    LowerLimit = vendorItem.LowerLimit,
                    Quantity = vendorItem.Quantity,
                    UpperLimit = vendorItem.UpperLimit,
                    IsActive = true

                };

                context.VendorItems.Add(VendorItemToSave);
            }
            await context.SaveChangesAsync();
            return "Saved";
        }

        public async Task<string> SaveItems(ItemDto Items)
        {

            var ItemToSave = new Item
            {


                Name = Items.Name,
                Description = Items.Description,
                Type = Items.Type,
                IsActive = Items.IsActive,
                AddedDate = DateTime.Now,
                ModifiedDate = DateTime.Now

                };

                context.Items.Add(ItemToSave);
         
            await context.SaveChangesAsync();
            return "Saved";
        }


        public async Task<string> GetRefCode()
        {

            var queryString = @"declare @code varchar(6)
                                    declare @rounds int = 1
                                    set @code = left( newid(), 6)

                                    while exists ( select * from vendors where RefCode like  @code + '%' )
                                        begin
                                            set @code = left( newid(), 6)
                                            set @rounds= @rounds + 1
                                        end

                                        select @code ";

            var rawQuery = context.Database.SqlQuery<String>(queryString);

            var strCode = await rawQuery.ToListAsync();

            if (strCode != null && strCode.Any())
                return strCode.FirstOrDefault()?.ToUpper();

            return string.Empty;
        }

        public async Task<GetCategoryResponseDto> GetAllActiveCategories()
        {
            GetCategoryResponseDto getCategoryResponseDto = new GetCategoryResponseDto();
            getCategoryResponseDto.ActiveCategories = new List<CategoryDto>();
            try
            {
                var activeCategories = context.Categories.Where(r => r.IsActive == true).ToList();
                foreach(var res in activeCategories)
                {
                    CategoryDto categoryDto  = new CategoryDto();
                    categoryDto.CategoryId = res.CategoryId;
                    categoryDto.Description = res.Description;
                    categoryDto.Name = res.Name;
                    categoryDto.IsActive = res.IsActive;
                    categoryDto.ImageUrl = res.ImageUrl;
                    getCategoryResponseDto.ActiveCategories.Add(categoryDto);

                }

                getCategoryResponseDto.ResponseCode = "00";
                getCategoryResponseDto.ResponseDescription = "Categories found";

            }
            catch(Exception ex)
            {
                
                getCategoryResponseDto.ResponseCode = "40";
                getCategoryResponseDto.ResponseDescription = "Categories not found. An error occured";
            }
          
            return getCategoryResponseDto;
        }


        public async Task<PriceRangeResDTO> GetAllPriceRange()
        {
            PriceRangeResDTO priceRangeResDTO = new PriceRangeResDTO();
            priceRangeResDTO.PriceRanges = new List<PriceRangeDTO>();
            try
            {
                var activePrices = context.PriceRanges.Where(r => r.IsActive == true).ToList();
                foreach (var res in activePrices)
                {
                    PriceRangeDTO priceRangeDTO = new PriceRangeDTO();
                    priceRangeDTO.PriceRangeId = res.PriceRangeId;
                    priceRangeDTO.LowerLimit = res.LowerLimit;
                    priceRangeDTO.UpperLimit = res.UpperLimit;
                    priceRangeDTO.IsActive = res.IsActive;
                    priceRangeResDTO.PriceRanges.Add(priceRangeDTO);

                }

                priceRangeResDTO.ResponseCode = "00";
                priceRangeResDTO.ResponseDescription = "Prices found";

            }
            catch (Exception ex)
            {

                priceRangeResDTO.ResponseCode = "40";
                priceRangeResDTO.ResponseDescription = "Prices not found. An error occured";
            }

            return priceRangeResDTO;
        }

        public Task<List<VendorDto>> GetVendorItemConfigurationWithPrice(VendorItemRequestModel vendorItemRequestModel)
        {
            var Vendors =
                  from vendorItems in context.VendorItems
                  join vendors in context.Vendors on vendorItems.VendorId equals vendors.VendorId
                  where vendorItems.ItemId == vendorItemRequestModel.ItemId
                   && vendorItems.LowerLimit <= vendorItemRequestModel.LowerLimit
                   && vendorItems.UpperLimit <= vendorItemRequestModel.UpperLimit
                  && vendorItems.IsActive == true

                  select new VendorDto
                  {
                      Id = vendors.VendorId,
                      Name = vendors.Name,
                      EmailAddress = vendors.EmailAddress,
                      MobileNo = vendors.MobileNo,
                      RefCode = vendors.RefCode,
                      Instagram = vendors.Instagram,
                      OfficeAddress = vendors.OfficeAddress
                  };


            return Vendors.ToListAsync();
        }


    }
}
