﻿using PlanPaddy.Core.DTOs;
using PlanPaddy.Core.Entities.Item;
using PlanPaddy.Core.Persistence.Repository.Vendors;
using PlanPaddy.Repository.Implementation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Repository.Vendors
{
   public class VendorRepository: Repository<Vendor>, IVendorRepository
    {
        public VendorRepository(PlanPaddyDataContext context) : base(context)
        {
        }



        public async Task<Vendor> FindExistingVendor(string email)
        {
            var existingVendor = context.Vendors.Where(v => v.EmailAddress == email).FirstOrDefault();
            return existingVendor;
        }

        public async Task<Vendor> SignIn(string email, string password)
        {
            var existingVendor = context.Vendors.Where(v => v.EmailAddress == email && v.Password == password).FirstOrDefault();
            return existingVendor;
        }

        public async Task<Vendor> FindExistingVendorById(Guid Id)
        {
            var existingVendor = context.Vendors.Where(v => v.VendorId == Id).FirstOrDefault();
            return existingVendor;
        }
        public async Task<string> GetRefCode()
        {

            var queryString = @"declare @code varchar(6)
                                    declare @rounds int = 1
                                    set @code = left( newid(), 6)

                                    while exists ( select * from vendors where RefCode like  @code + '%' )
                                        begin
                                            set @code = left( newid(), 6)
                                            set @rounds= @rounds + 1
                                        end

                                        select @code ";

            var rawQuery = context.Database.SqlQuery<String>(queryString);

            var strCode = await rawQuery.ToListAsync();

            if (strCode != null && strCode.Any())
                return strCode.FirstOrDefault()?.ToUpper();

            return string.Empty;
        }
        public async Task<VendorDto> SaveVendor(VendorPostRequest vendorDto)
        {
           
            var VendorToSave = new Vendor
            {
                Name = vendorDto.Name,
                FirstName = vendorDto.FirstName,
                LastName = vendorDto.LastName,
                Description = vendorDto.Description,
                Picture = vendorDto.Picture,
                EmailAddress = vendorDto.EmailAddress,
                MobileNo = vendorDto.MobileNo,
                Password= vendorDto.Password,
                IsActive = true,
                OTP="",
                SubscriptionPlan= vendorDto.SubscriptionPlan,
                ExpiryDate = DateTime.Now.AddDays(365),
                Instagram = vendorDto.Instagram,
                Facebook = vendorDto.Facebook,
                Twitter = vendorDto.Twitter,
                OfficeAddress = vendorDto.OfficeAddress,
                Verified = vendorDto.Verified,
                AddedDate=DateTime.Now,
                ModifiedDate=DateTime.Now,
                AccountIsConfirmed= true

            };
            var refcode = await GetRefCode();
            refcode += "-VEN";
            VendorToSave.RefCode = refcode;
            context.Vendors.Add(VendorToSave);
            await context.SaveChangesAsync();

            var savedVendor = await GetVendorByRefCode(VendorToSave.RefCode);
            var savedVendorDto = new VendorDto
            {
                Id = VendorToSave.VendorId,
                Name = savedVendor.Name,
                Description = savedVendor.Description,
                Picture = savedVendor.Picture,
                EmailAddress = savedVendor.EmailAddress,
                MobileNo = savedVendor.MobileNo,
                IsActive = savedVendor.IsActive,
                RefCode = savedVendor.RefCode,
            };
            return savedVendorDto;
        }


        public async Task<string> SaveVendorItems(List<VendorItemDto> vendorItems)
        {
            foreach (var vendorItem in vendorItems)
            {
                var VendorItemToSave = new VendorItem
                {

                    ItemId = vendorItem.ItemId,
                    VendorId = vendorItem.VendorId,
                    CategoryId = vendorItem.CategoryId,
                    LowerLimit = vendorItem.LowerLimit,
                    Quantity = vendorItem.Quantity,
                    UpperLimit = vendorItem.UpperLimit,
                    Description=vendorItem.Description,
                    IsActive = true,
                    AddedDate=DateTime.Now,
                    ModifiedDate= DateTime.Now

                };

                context.VendorItems.Add(VendorItemToSave);
            }
            await context.SaveChangesAsync();
            return "Saved";
        }
        public async Task<Vendor> GetVendorByRefCode(string refCode)
        {
            var VendorObj = await context.Vendors.Where(r => r.RefCode == refCode).FirstOrDefaultAsync();
            return VendorObj;
        }

        public async Task<Vendor> GetVendorByEmail(string email)
        {
            var VendorObj = await context.Vendors.Where(r => r.EmailAddress == email).FirstOrDefaultAsync();
            return VendorObj;
        }
        public async Task<List<VendorItem>> GetVendorItemsByVendorId(Guid vendorId)
        {
            var VendorItemObj = context.VendorItems.Where(r => r.VendorId == vendorId);

            return VendorItemObj.ToList();
        }
        public async Task<List<Vendor>> GetAllActiveVendors()
        {
            var activeVendors = context.Vendors.Where(r => r.IsActive == true);
            return activeVendors.ToList();
        }

        public async Task<List<Vendor>> GetAllInActiveVendors()
        {
            var InActiveVendors = context.Vendors.Where(r => r.IsActive == false);
            return InActiveVendors.ToList();
        }

        public async Task<Vendor> UpdateVendor(VendorDto vendorDto)
        {
            var vendor = await context.Vendors.FirstOrDefaultAsync(r => r.RefCode == vendorDto.RefCode);
            vendor.Name = vendorDto.Name;
            vendor.Description = vendorDto.Description;
            vendor.Picture = vendorDto.Picture;
            vendor.EmailAddress = vendorDto.EmailAddress;
            vendor.MobileNo = vendorDto.MobileNo;
            //vendor.IsActive = true;
            await context.SaveChangesAsync();
            vendor = await context.Vendors.FirstOrDefaultAsync(r => r.RefCode == vendorDto.RefCode);
            return vendor;
        }

        public async Task<Vendor> UpdateVendorOtp(VendorDto vendorDto)
        {
            var vendor = await context.Vendors.FirstOrDefaultAsync(r => r.EmailAddress == vendorDto.EmailAddress);
            vendor.OTP = vendorDto.OTP;
            vendor.AccountIsConfirmed = vendorDto.AccountIsConfirmed;
            //vendor.IsActive = true;
            await context.SaveChangesAsync();
            vendor = await context.Vendors.FirstOrDefaultAsync(r => r.RefCode == vendorDto.RefCode);
            return vendor;
        }

        public async Task<Vendor> UpdateVendorDetails(VendorDto vendorDto)
        {
            var vendor = await context.Vendors.FirstOrDefaultAsync(r => r.EmailAddress == vendorDto.EmailAddress);
            vendor.MobileNo = vendorDto.MobileNo;
            vendor.Description = vendorDto.Description;
            vendor.Twitter = vendorDto.Twitter;
            vendor.Facebook = vendorDto.Facebook;
            vendor.Instagram = vendorDto.Instagram;
            //vendor.IsActive = true;
            await context.SaveChangesAsync();
            vendor = await context.Vendors.FirstOrDefaultAsync(r => r.RefCode == vendorDto.RefCode);
            return vendor;
        }
        public async Task<Vendor> EditVendor(VendorDto vendorDto)
        {
            var vendor = await context.Vendors.FirstOrDefaultAsync(r => r.RefCode == vendorDto.RefCode);
            vendor.Name = vendorDto.Name;
            vendor.Description = vendorDto.Description;
            vendor.Picture = vendorDto.Picture;
            vendor.EmailAddress = vendorDto.EmailAddress;
            vendor.MobileNo = vendorDto.MobileNo;
            vendor.IsActive = vendorDto.IsActive;
            await context.SaveChangesAsync();
            vendor = await context.Vendors.FirstOrDefaultAsync(r => r.RefCode == vendorDto.RefCode);
            return vendor;
        }

        public async Task<Vendor> ResetPassord(VendorDto vendorDto)
        {
            var vendor = await context.Vendors.FirstOrDefaultAsync(r => r.EmailAddress == vendorDto.EmailAddress);
            vendor.Password = vendorDto.Password;
            vendor.IsTempPassword = vendorDto.IsTempPassword;
            await context.SaveChangesAsync();
            vendor = await context.Vendors.FirstOrDefaultAsync(r => r.EmailAddress == vendorDto.EmailAddress);
            return vendor;
        }
    }
}
