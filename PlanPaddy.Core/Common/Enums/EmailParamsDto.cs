﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Common.Enums
{
  public  class EmailParamsDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        // public int Port { get; set; }
        public string Host { get; set; }
        public bool UseSsl { get; set; }
    }
}
