﻿using PlanPaddy.Core.Cryptography;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Common.Extensions
{
  public static  class StringExtensions
    {

        public static string Encrypt(this string textToEncrypt)
        {
            return CryptManager.Encrypt(textToEncrypt);
        }

        public static string Derypt(this string textToDecrypt)
        {
            return CryptManager.Decrypt(textToDecrypt);
        }
    }


}
