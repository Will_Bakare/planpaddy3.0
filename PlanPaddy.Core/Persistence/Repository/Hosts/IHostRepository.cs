﻿using PlanPaddy.Core.DTOs;
using PlanPaddy.Core.Entities.Host;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Persistence.Repository.Hosts
{
   public interface IHostRepository : IRepository<Host>
    {
        Task<HostDto> SaveHost(HostDto Host);
        Task<Host> GetHost(string HostId);
    }
}
