﻿using PlanPaddy.Core.DTOs;
using PlanPaddy.Core.Entities.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Persistence.Repository.Vendors
{
   public interface IVendorRepository:IRepository<Vendor>
    {
        Task<VendorDto> SaveVendor(VendorPostRequest vendorDto);
        Task<string> SaveVendorItems(List<VendorItemDto> vendorItems);
        Task<Vendor> GetVendorByRefCode(string refCode);
        Task<Vendor> GetVendorByEmail(string refCode);
        Task<List<VendorItem>> GetVendorItemsByVendorId(Guid vendorId);
        Task<Vendor> EditVendor(VendorDto vendorDto);
        Task<List<Vendor>> GetAllActiveVendors();
        Task<List<Vendor>> GetAllInActiveVendors();
        Task<Vendor> FindExistingVendor(string email);
        Task<Vendor> FindExistingVendorById(Guid Id);
        Task<Vendor> UpdateVendorOtp(VendorDto vendorDto);
        Task<Vendor> SignIn(string email, string password);
        Task<Vendor> UpdateVendorDetails(VendorDto vendorDto);
        Task<Vendor> ResetPassord(VendorDto vendorDto);
    }
}
