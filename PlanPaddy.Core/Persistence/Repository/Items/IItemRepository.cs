﻿using PlanPaddy.Core.DTOs;
using PlanPaddy.Core.Entities;
using PlanPaddy.Core.Entities.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Persistence.Repository.Items
{
    public interface IItemRepository:IRepository<Item>
    {
        
        Task<List<ItemDto>> GetItemsByCategory(Guid categoryId);
        Task<List<ItemDto>> GetItemsConfigurationByCategoryId(Guid categoryId);
        Task<List<VendorDto>> GetVendorItemConfiguration(VendorItemRequestModel vendorItemRequestModel);
        Task<bool> SaveQuote(string quoteJsonSTring);
        Task<Quote> GetQuoteById(Guid quoteId);
        //Task<VendorDto> SaveVendor(VendorDto vendorDto);
        //Task<string> SaveVendorItems(List<VendorItemDto> vendorItems);
       // Task<Vendor> GetVendorByRefCode(string refCode);
       // Task<List<VendorItem>> GetVendorItemsByVendorId(Guid vendorId);
         //Task<List<Vendor>> GetAllActiveVendors();
         //Task<List<Vendor>> GetAllInActiveVendors();
       // Task<Vendor> EditVendor(VendorDto vendorDto);
        Task<string> SaveItems(ItemDto Items);
        Task<ItemConfigurationDto> CreateItemConfiguration(ItemConfigurationDto itemConfigurationDto);
        Task<List<Item>> GetAllItems();
        Task<GetCategoryResponseDto> GetAllActiveCategories();
        Task<PriceRangeResDTO> GetAllPriceRange();
        Task<List<VendorDto>> GetVendorItemConfigurationWithPrice(VendorItemRequestModel vendorItemRequestModel);
        void Dispose();
    }
}
