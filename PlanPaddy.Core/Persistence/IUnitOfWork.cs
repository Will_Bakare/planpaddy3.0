﻿using PlanPaddy.Core.Persistence.Repository.Hosts;
using PlanPaddy.Core.Persistence.Repository.Items;
using PlanPaddy.Core.Persistence.Repository.Vendors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Persistence
{
   public interface IUnitOfWork
    {
        
        IItemRepository Items { get; set; }
        IVendorRepository Vendors { get; set; }

        IHostRepository Hosts { get; set; }
        Task CompleteAsync();
    }
}
