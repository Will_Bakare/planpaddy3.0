﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.DTOs
{
   public  class BaseResponse
    {
        public string ResponseDescription { get; set; }
        public string ResponseCode{ get; set; }
    }
}
