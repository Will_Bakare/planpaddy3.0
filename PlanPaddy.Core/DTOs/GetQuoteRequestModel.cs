﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.DTOs
{
    public class GetQuoteRequestModel
    {
        public Guid CategoryId { get; set; }
        public double LowerLimit { get; set; }
        public double UpperLimit { get; set; }

    }
}
