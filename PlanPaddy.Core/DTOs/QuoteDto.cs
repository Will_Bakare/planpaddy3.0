﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.DTOs
{
    public class QuoteDto
    {
        public Guid QuoteId { get; set; }
        public string QuoteJsonString { get; set; }
    }
}
