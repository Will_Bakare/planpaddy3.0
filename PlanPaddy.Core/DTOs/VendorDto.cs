﻿using PlanPaddy.Core.Entities.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.DTOs
{
    public class VendorDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }

        public string Picture { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNo { get; set; }
        public string RefCode { get; set; }
        public bool IsActive { get; set; }
       public string Username { get; set; }
        public string Password { get; set; }
        public string OTP { get; set; }
        public string SubscriptionPlan { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string Instagram { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string OfficeAddress { get; set; }
        public string Verified { get; set; }
        public bool AccountIsConfirmed { get; set; }
        public bool IsTempPassword { get; set; }
        public List<VendorItemDto> VendorItems {get; set;}

    }
}
