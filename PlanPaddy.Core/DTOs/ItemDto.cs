﻿using PlanPaddy.Core.Common.Enums;
using PlanPaddy.Core.Entities.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.DTOs
{
    public class ItemDto
    {
        public Guid ItemId { get; set; }
        public Guid CategoryId { get; set; }
        public double? ItemPercentage { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ItemType Type { get; set; }
        public double LowerLimit { get; set; }

        public double UpperLimit { get; set; }
        public List<VendorDto> Vendors { get; set; }
        public bool IsActive { get; set; }
        public string ImageUrl { get; set; }
    }

    public class ItemDVendorByPrice
    {
        public Guid ItemId { get; set; }
      
        //public double? ItemPercentage { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public ItemType Type { get; set; }
        public double LowerLimit { get; set; }

        public double UpperLimit { get; set; }
        //public List<VendorDto> Vendors { get; set; }
        //public bool IsActive { get; set; }
    }

    public class ItemDVendorByPriceRes
    {
        public Guid ItemId { get; set; }
        public double LowerLimit { get; set; }
        public double UpperLimit { get; set; }
        public List<VendorDto> Vendors { get; set; }
    }

    public class ItemDVendorByPriceList
        {
        public Guid CategoryId { get; set; }
        public List<ItemDVendorByPrice> Items { get; set; }
        }

    public class ItemDVendorByPriceResList
    {
        public Guid CategoryId { get; set; }
        public List<ItemDVendorByPriceRes> ItemsWithVendors { get; set; }
    }
}
