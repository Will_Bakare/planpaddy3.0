﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.DTOs
{
    public class PriceRangeDTO
    {
        public Guid PriceRangeId { get; set; }
        public double LowerLimit { get; set; }
        public double UpperLimit { get; set; }
        public bool IsActive { get; set; }
    }

    public class PriceRangeResDTO:BaseResponse
    {
        
        public List<PriceRangeDTO> PriceRanges { get; set; }
    }
}
