﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.DTOs
{
    public class ItemConfigurationDto
    {

        public Guid? ItemConfigurationId { get; set; }
        public Guid CategoryId { get; set; }
        public Guid ItemId { get; set; }
        public double ItemPercentage { get; set; }
        public bool IsActive { get; set; }
    }
}
