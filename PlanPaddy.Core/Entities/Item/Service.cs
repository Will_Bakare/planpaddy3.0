﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Entities.Item
{
   public class Service : BaseEntity
    {
        public Guid ServiceId { get; set; }
        public string ServiceName { get; set; }

    }
}
