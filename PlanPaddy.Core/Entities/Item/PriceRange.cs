﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Entities.Item
{
   public class PriceRange : BaseEntity
    {

        public PriceRange()
        {
            PriceRangeId = Guid.NewGuid();
        }
        public Guid PriceRangeId { get; set; }
        public double LowerLimit { get; set; }

        public double UpperLimit { get; set; }

        public bool IsActive { get; set; }
    }
}
