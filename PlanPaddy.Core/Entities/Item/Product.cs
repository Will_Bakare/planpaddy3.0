﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Entities.Item
{
    public class Product : BaseEntity
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
    }
}
