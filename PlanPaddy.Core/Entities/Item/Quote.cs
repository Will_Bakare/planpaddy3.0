﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Entities.Item
{
    public class Quote : BaseEntity
    {
        public Quote()
        {
            QuoteId = Guid.NewGuid();
        }
        public Guid QuoteId { get; set; }
        public string QuoteJsonString { get; set; }

    }
}
