﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Entities.Item
{
   public class ItemCategory : BaseEntity
    {
        public ItemCategory()
        {
            ItemCategoryId = Guid.NewGuid();
        }
        public Guid ItemCategoryId { get; set; }
        public Guid ItemId { get; set; }
        public Guid CategoryId { get; set; }
        public bool IsActive { get; set; }

        //FK Constraints
        [ForeignKey("ItemId")]
        public Item Item { get; set; }
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
    }
}
