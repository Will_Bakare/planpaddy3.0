﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace PlanPaddy.Core.Entities.Item
{
   public class ItemConfiguration : BaseEntity
    {
        public ItemConfiguration()
        {
            ItemConfigurationId = Guid.NewGuid();
        }
        public Guid ItemConfigurationId { get; set; }
        public Guid CategoryId { get; set; }
        public Guid ItemId { get; set; }
        public double ItemPercentage { get; set; }
        public bool IsActive { get; set; }

        //FK Constraints
        [ForeignKey("ItemId")]
        public Item Item { get; set; }
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }

    }
}
