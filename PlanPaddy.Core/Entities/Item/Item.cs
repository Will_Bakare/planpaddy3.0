﻿using PlanPaddy.Core.Entities;
using PlanPaddy.Core.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Entities.Item
{
   public class Item : BaseEntity
    {
        public Item()
        {
            ItemId = Guid.NewGuid();
        }
        public Guid ItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ItemType Type { get; set; }
        public bool IsActive { get; set; }
        public string ImageUrl { get; set; }


    }
}
