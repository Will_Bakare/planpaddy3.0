﻿using PlanPaddy.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Entities.Item
{
    public class Category : BaseEntity
    {
        public Category()
        {
            CategoryId = Guid.NewGuid();
        }
        public Guid CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string ImageUrl { get; set; }

    }
}
