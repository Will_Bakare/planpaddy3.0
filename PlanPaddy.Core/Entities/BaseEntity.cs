﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlanPaddy.Core.Entities
{
    public class BaseEntity
    {
        public DateTime AddedDate { get; set; }

        public DateTime ModifiedDate { get; set; }
    }
}
