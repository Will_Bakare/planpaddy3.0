﻿using PlanPaddy.Core.DTOs;
using PlanPaddy.Core.Entities.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Services.Vendor
{
    public interface IVendorService
    {
        #region Vendors
        //done
        Task<VendorDto> PostVendor(VendorPostRequest vendorDto);
        Task<VendorDto> PostVendorItems(VendorDto vendorDto);
        Task<VendorDto> SignIn(VendorDto vendorDto);
        Task<VendorDto> EditVendor(VendorDto vendorDto);
        //done
        Task<VendorDto> GetVendorDetailsByRefCode(string refCode);
        Task<List<VendorDto>> GetAllVendors();
        Task<VendorDto> ConfirmActivationCode(VendorDto vendorDto);
        Task<VendorDto> GetVendorDetailsByEmail(string email);
        Task<VendorDto> UpdateVendorDetails(VendorDto vendorDto);
        Task<VendorDto> MocMail(VendorDto vendor);
        Task<VendorDto> RequestResetPassword(VendorDto vendorDto);
        Task<VendorDto> ResetPassword(VendorDto vendorDto);
        Task<VendorDto> ResendVerificationCode(VendorPostRequest vendorDto);



        #endregion
    }
}
