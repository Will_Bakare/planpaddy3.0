﻿using PlanPaddy.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Services.Item
{
   public interface IItemService 
    {
        #region Quotes
        Task<List<ItemDto>> GetQuote(GetQuoteRequestModel getQuoteRequestModel);
        //Done
        Task<QuoteDto>SendQuote(SendQuoteRequestModel sendQuoteRequestModel);
        //Done but we need to work on email body and host
        Task<bool> SaveQuote(string QuoteJsonSTring);
        #endregion

        #region Items
        Task<string> PostItem(ItemDto itemDto);
        Task<ItemDto> EditItem(GetQuoteRequestModel getQuoteRequestModel);
        Task<ItemDto> GetItemById(GetQuoteRequestModel getQuoteRequestModel);
        Task<List<ItemDto>> GetAllItems();
        #endregion

        //#region Vendors
        ////done
        //Task<VendorDto> PostVendor(VendorDto vendorDto);
        //Task<VendorDto> EditVendor(VendorDto vendorDto);
        ////done
        //Task<VendorDto> GetVendorDetailsByRefCode(string refCode);
        //Task<List<VendorDto>> GetAllVendors();
       
        //#endregion

        #region Categories
        Task<CategoryDto> PostCategory(GetQuoteRequestModel getQuoteRequestModel);
        Task<CategoryDto> EditCategory(GetQuoteRequestModel getQuoteRequestModel);
        Task<CategoryDto> GetCategoryById(GetQuoteRequestModel getQuoteRequestModel);
        Task<PriceRangeResDTO> GetAllPriceRange();
      //  Task<List<CategoryDto>> GetAllCategories(GetQuoteRequestModel getQuoteRequestModel);
        Task<GetCategoryResponseDto> GetAllActiveCategories();
        #endregion

        #region ItemConfiguration
        Task<ItemConfigurationDto> CreateItemConfiguration(ItemConfigurationDto itemConfigurationDto);
        Task<List<ItemDto>> GetItemsConfigurationByCategoryId(Guid CategoryId);
        Task<ItemDVendorByPriceResList> GetQuoteByVendorPrice(ItemDVendorByPriceList itemDVendorByPrice);
        #endregion
    }
}
