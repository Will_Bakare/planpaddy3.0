﻿using PlanPaddy.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanPaddy.Core.Services.Host
{
    public interface IHostService
    {
        Task<HostDto> SaveHost(HostDto Host);
        Task<HostDto> GetHost(string HostId);
    }
}
